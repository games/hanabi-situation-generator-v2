USER_ID=$(shell id -u)
GROUP_ID=$(shell id -g)
COMPOSE_FILE=./docker/compose.yml

.DEFAULT_GOAL := help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-30s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

docker-compose-user-exec = docker compose -f ${COMPOSE_FILE} exec -T --user ${USER_ID}:${GROUP_ID} ${1}
docker-compose-root-exec = docker compose -f ${COMPOSE_FILE} exec -T ${1}

# -------------
# Lancement des containers
# -------------

up: ## Lancement de l'environnement de dev pour le fronted
	docker compose -f ${COMPOSE_FILE} up

down: ## Arrêt de l'environnement de dev pour le frontend
	docker compose -f ${COMPOSE_FILE} down

# -------------
# Construction de l’image docker
# -------------

image-build: ## Reconstruction de l’image frontend pour le dev
	./docker/build.sh base --no-push

## -------------
## Outils
## -------------

exec-cmd: ## Éxécution d’une commande bash dans le conteneur (make exec-cmd CMD="ls .")
	$(call docker-compose-user-exec, react $(CMD))

exec-bash: ## Lancement du terminal dans le conteneur
	docker compose -f ${COMPOSE_FILE} exec --user ${USER_ID}:${GROUP_ID} react bash

build: ## Transpilage de l’application
	$(call docker-compose-root-exec, react npm run build)

package: ## Empaquetage de l’application sous forme d’archive
	mkdir -p ci_build
	docker run -i --rm -v $(shell pwd):/srv/sources:rw registry.gitlab.com/v.nivuahc/docker-tools/package:latest

build-and-package: build package ## Transpilage et empaquetage de l’application sous forme d’archive
