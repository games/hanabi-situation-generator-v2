Hanabi Situation Generator v2
=============================

This application is made for the players of the game Hanabi.

When you are discussing a particular situation that occurs in the game, it is useful to illustrate it.
This application allows you to represent the game situation, and to share it as an image that you can download,
or as a link that you can copy-paste.

There are a few other features:

- You can choose the color sets to use
- You can convert the old URLs of the old version of the generator to the new one
- You can copy-paste a text version of the situation

## Requirements

- Docker

## Installation

The first time, you need to build the docker image:

```bash
make image-build
```

## Start

To start the application:

```bash
make up
```

This will install all the dependencies with **npm**, and run the dev environment.

The application is now running on <http://localhost:4300>


## Build

To build the application, you need to run

```bash
make build-and-package
```

Il will transpile and create an archive of the application, that you can deploy on your webserver.
You will find this archive here: `ci_build/hanabi-sg.zip`
