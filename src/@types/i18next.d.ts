import 'i18next';
import enTranslations from '../locales/en/translation.json';
import frTranslations from '../locales/fr/translation.json';

declare module 'i18next' {
  interface CustomTypeOptions {
    defaultNS: 'translation';
    resources: {
      en: typeof enTranslations;
      fr: typeof frTranslations;
    };
  }
}
