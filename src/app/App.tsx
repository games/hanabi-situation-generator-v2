import { useRedirectToV2 } from '@/features/redirector/hooks/useRedirectToV2';
import { AppBar } from '@/layout/Toolbar/AppBar';
import { NotificationSnack } from '@/shared/components/NotificationSnack/NotificationSnack';
import { CssBaseline } from '@mui/material';
import Link from '@mui/material/Link';
import React, { useState } from 'react';
import './i18n';
import { Trans } from 'react-i18next';
import { BrowserRouter, Link as RouterLink } from 'react-router-dom';
import { Routing } from './Routing';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

export const App = () => {
  const { redirected } = useRedirectToV2();

  const [redirectInfo, setRedirectInfo] = useState(redirected);

  return (
    <BrowserRouter>
      <CssBaseline />
      <AppBar />
      <DndProvider backend={HTML5Backend}>
        <Routing />
      </DndProvider>

      <NotificationSnack opened={redirectInfo} onDismiss={() => setRedirectInfo(false)} type="info">
        <Trans i18nKey="situation.redirectInfo">
          You were redirected from the old URL. You can use the
          <Link
            component={RouterLink}
            to="/url-converter"
            sx={{ color: '#FFF', textDecoration: 'underline' }}
          >
            converter
          </Link>{' '}
          to get the new URLs.
        </Trans>
      </NotificationSnack>
    </BrowserRouter>
  );
};
