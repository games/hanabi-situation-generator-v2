import { Configurator } from '@/pages/Configurator/Configurator';
import { Converter } from '@/pages/Converter/Converter';
import React from 'react';
import { Route, Routes } from 'react-router-dom';

export const Routing = () => {
  return (
    <Routes>
      <Route path="/:base64SituationData" element={<Configurator />} />
      <Route path="/url-converter" element={<Converter />} />
      <Route path="*" element={<Configurator />} />
    </Routes>
  );
};
