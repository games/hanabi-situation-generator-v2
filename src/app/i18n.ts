import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import enTranslations from '../locales/en/translation.json';
import frTranslations from '../locales/fr/translation.json';

export const defaultNS = 'translation';
export const resources = {
  en: { [defaultNS]: enTranslations },
  fr: { [defaultNS]: frTranslations },
} as const;

i18next
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    defaultNS,
    detection: { order: ['navigator'] },
    fallbackLng: 'en',
    supportedLngs: Object.keys(resources),
    keySeparator: '.',
    debug: true,
    interpolation: {
      escapeValue: false,
    },
  });
