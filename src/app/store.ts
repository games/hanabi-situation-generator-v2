import { SettingService } from '@/features/settings/services/SettingService.ts';
import {
  settingReducer,
  setColorSet,
  toggleCardDrawDirection,
  toggleFormClosureMode,
  toggleHideDeck,
  setDefaultNbOfPlayers,
} from '@/features/settings/slices/settingSlice.ts';
import { deckReducer } from '@/features/configurator/slices/deckSlice';
import { discardPileReducer } from '@/features/configurator/slices/discardPileSlice';
import { fireworksReducer } from '@/features/configurator/slices/fireworksSlice';
import { playersReducer } from '@/features/configurator/slices/playersSlice';
import { configureStore, createListenerMiddleware, isAnyOf } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';

export type RootState = ReturnType<typeof store.getState>;
const { saveToStorage } = SettingService();

const localStorageMiddleware = createListenerMiddleware();
localStorageMiddleware.startListening({
  matcher: isAnyOf(
    toggleHideDeck,
    setColorSet,
    toggleCardDrawDirection,
    toggleFormClosureMode,
    setDefaultNbOfPlayers,
  ),
  effect: (_action, listenerApi) => {
    saveToStorage((listenerApi.getState() as RootState).settings);
  },
});

const store = configureStore({
  reducer: {
    deck: deckReducer,
    discardPile: discardPileReducer,
    fireworks: fireworksReducer,
    players: playersReducer,
    settings: settingReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().prepend(localStorageMiddleware.middleware),
});

export type AppDispatch = typeof store.dispatch;

export const useAppDispatch: () => AppDispatch = useDispatch;

export default store;
export const useAppSelector = useSelector.withTypes<RootState>();
