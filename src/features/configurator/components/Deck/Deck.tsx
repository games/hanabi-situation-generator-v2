import { DeckEdit } from '@/features/configurator/components/Deck/DeckEdit/DeckEdit';
import { useDeck } from '@/features/configurator/components/Deck/useDeck';
import { CardSimple } from '@/features/situation/components/CardSimple/CardSimple';
import { CardWrapper } from '@/features/situation/components/CardWrapper/CardWrapper';
import { EmptySlot } from '@/features/situation/components/EmptySlot/EmptySlot';
import { ActionMenu } from '@/shared/components/ActionMenu/ActionMenu';
import { RemoveButton } from '@/shared/components/buttons/RemoveButton/RemoveButton';
import { FlexWrapper } from '@/shared/components/FlexWrapper/FlexWrapper';
import { Stack } from '@mui/material';
import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHover } from 'usehooks-ts';
import styles from './deck.module.css';

type Props = {
  readonly?: boolean;
};

export const Deck = ({ readonly = false }: Props) => {
  const { t } = useTranslation();

  const { deck, onRemove } = useDeck();
  const [displayForm, setDisplayForm] = useState(false);

  const container = useRef(null);
  const isHovered = useHover(container);

  const displayDeck = deck !== undefined;

  return (
    <>
      {displayDeck && (
        <>
          {readonly && deck !== undefined ? (
            <CardWrapper>
              <CardSimple
                value={deck.nbRemainingCards}
                onClick={readonly ? undefined : () => setDisplayForm(true)}
                title={t('deck.action.edit')}
              />
            </CardWrapper>
          ) : (
            <Stack ref={container} className={`${styles.container} flexBox`} spacing={1}>
              <span>{t('deck.label')}</span>

              <ActionMenu opened={isHovered && deck !== undefined} vertical={false} outside={false}>
                <RemoveButton onClick={onRemove} title={t('deck.action.remove')} />
              </ActionMenu>

              {displayForm ? (
                <CardWrapper>
                  <FlexWrapper>
                    <DeckEdit
                      deck={deck ?? { nbRemainingCards: 1 }}
                      onBlur={() => {
                        setDisplayForm(false);
                      }}
                    />
                  </FlexWrapper>
                </CardWrapper>
              ) : (
                <CardWrapper>
                  {deck === undefined ? (
                    <EmptySlot onClick={() => setDisplayForm(true)} title={t('deck.action.edit')} />
                  ) : (
                    <CardSimple
                      value={deck.nbRemainingCards ?? { nbRemainingCards: 1 }}
                      onClick={() => setDisplayForm(true)}
                      title={t('deck.action.edit')}
                    />
                  )}
                </CardWrapper>
              )}
            </Stack>
          )}
        </>
      )}
    </>
  );
};
