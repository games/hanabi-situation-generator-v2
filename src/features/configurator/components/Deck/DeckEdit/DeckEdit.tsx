import { useDeckEdit } from '@/features/configurator/components/Deck/DeckEdit/useDeckEdit';
import { NumberField } from '@/features/situation/components/fields/NumberField/NumberField';
import { Deck as DeckModel } from '@/features/situation/model/deck/Deck.interface';
import { MAX_DECK_SIZE } from '@/features/situation/model/deck/MaxDeckSize.const';
import { Stack } from '@mui/material';
import styles from './deckEdit.module.css';

type Props = {
  deck: DeckModel;
  onChange?: () => void;
  onBlur?: () => void;
};

export const DeckEdit = ({ deck, onChange, onBlur }: Props) => {
  const { setNbRemainingCards } = useDeckEdit();
  return (
    <Stack className={styles.container} spacing={1}>
      <NumberField
        value={deck.nbRemainingCards || 1}
        onChange={(value: number | undefined) => {
          setNbRemainingCards(value);
          onChange && onChange();
        }}
        onBlur={onBlur}
        max={MAX_DECK_SIZE}
      />
    </Stack>
  );
};
