import { useAppDispatch } from '@/app/store';
import { setNbRemainingCards } from '@/features/configurator/slices/deckSlice';

export const useDeckEdit = () => {
  const dispatch = useAppDispatch();

  return {
    setNbRemainingCards: (nb: number | undefined) => dispatch(setNbRemainingCards(nb)),
  };
};
