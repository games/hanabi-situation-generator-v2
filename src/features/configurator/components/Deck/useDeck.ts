import { useAppDispatch } from '@/app/store';
import { selectDeck, setNbRemainingCards } from '@/features/configurator/slices/deckSlice';
import { useSelector } from 'react-redux';

export const useDeck = () => {
  const dispatch = useAppDispatch();

  const deck = useSelector(selectDeck);

  return {
    deck,
    onRemove: () => dispatch(setNbRemainingCards(undefined)),
  };
};
