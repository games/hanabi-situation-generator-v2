import { DiscardedColor } from '@/features/configurator/components/DiscardPile/DiscardedColor/DiscardedColor';
import { useDiscardPile } from '@/features/configurator/components/DiscardPile/useDiscardPile';
import { CardWrapper } from '@/features/situation/components/CardWrapper/CardWrapper';
import { EmptySlot } from '@/features/situation/components/EmptySlot/EmptySlot';
import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';
import { Stack } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { MdAdd } from 'react-icons/md';
import styles from './discardPile.module.css';

type Props = {
  readonly?: boolean;
};

export const DiscardPile = ({ readonly = false }: Props) => {
  const { t } = useTranslation();
  const { addColor, discardPile, nbOfColors, nbOfColorsInDiscardPile } = useDiscardPile();

  const displayDiscardPile = !readonly || Object.keys(discardPile).length > 0;

  return (
    <>
      {displayDiscardPile && (
        <Stack className={styles.container} spacing={1}>
          {!readonly && <span>{t('discardPile.label')}</span>}

          <Stack spacing={0.5}>
            {Object.entries(discardPile).map(
              ([color, values]: [string, Value[]], index: number) => (
                <DiscardedColor
                  key={index}
                  color={color as Color}
                  values={values}
                  readonly={readonly}
                />
              ),
            )}

            {!readonly && nbOfColorsInDiscardPile < nbOfColors && (
              <CardWrapper small>
                <EmptySlot
                  onClick={() => addColor()}
                  title={t('discardPile.action.add')}
                  emptyIcon={nbOfColorsInDiscardPile > 0 ? <MdAdd size={'70%'} /> : undefined}
                />
              </CardWrapper>
            )}
          </Stack>
        </Stack>
      )}
    </>
  );
};
