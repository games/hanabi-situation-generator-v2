import { colorsByColorSet } from '@/features/settings/helpers/color-mapping';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { ColorField } from '@/features/situation/components/fields/ColorField/ColorField';
import { ValueMultipleTextField } from '@/features/situation/components/fields/ValueMultipleTextField/ValueMultipleTextField';
import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';
import { FloatingForm } from '@/shared/components/FloatingForm/FloatingForm';
import { Stack } from '@mui/material';
import { useTranslation } from 'react-i18next';
import styles from './discardPileEdit.module.css';

type Props = {
  color: Color;
  colorSet: ColorSet;
  values: Value[];
  onChange?: (color: Color, values: Value[]) => void;
  onBlur?: () => void;
};

export const DiscardPileEdit = ({ color, colorSet, values, onChange, onBlur }: Props) => {
  const { t } = useTranslation();

  return (
    <FloatingForm onBlurOrDismiss={() => onBlur && onBlur()}>
      <Stack className={styles.container} spacing={1}>
        <ValueMultipleTextField
          label={t('value.labelAlt')}
          helperText={t('value.helper')}
          values={values}
          onBlur={(values: Value[]) => {
            onChange && onChange(color, values);
          }}
        />
        <ColorField
          label={t('color.label')}
          colors={colorsByColorSet(colorSet)}
          selected={color}
          onChange={(color: Color | undefined) => {
            onChange && color && onChange(color, values);
          }}
          allowUndefinedColor={false}
        />
      </Stack>
    </FloatingForm>
  );
};
