import { selectSettings } from '@/features/settings/slices/settingSlice.ts';
import { DiscardPileEdit } from '@/features/configurator/components/DiscardPile/DiscardPileEdit/DiscardPileEdit';
import { useDiscardPile } from '@/features/configurator/components/DiscardPile/useDiscardPile';
import { CardSimple } from '@/features/situation/components/CardSimple/CardSimple';
import { CardWrapper } from '@/features/situation/components/CardWrapper/CardWrapper';
import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';
import { ActionMenu } from '@/shared/components/ActionMenu/ActionMenu';
import { RemoveButton } from '@/shared/components/buttons/RemoveButton/RemoveButton';
import { Box, Stack } from '@mui/material';
import { blue } from '@mui/material/colors';
import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useHover } from 'usehooks-ts';
import styles from './discardedColor.module.css';

type Props = {
  color: Color;
  values: Value[];
  readonly?: boolean;
};

export const DiscardedColor = ({ color, values, readonly = false }: Props) => {
  const { t } = useTranslation();
  const { updateColorAndValues, removeColor } = useDiscardPile();

  const [displayForm, setDisplayForm] = useState(false);

  const container = useRef(null);
  const isHovered = useHover(container);

  const { colorSet } = useSelector(selectSettings);

  return (
    <Stack
      ref={container}
      className={styles.container}
      direction="row"
      spacing={0.5}
      sx={{ backgroundColor: !readonly && isHovered ? blue[50] : undefined }}
    >
      <CardWrapper small>
        <CardSimple
          onClick={readonly ? undefined : () => setDisplayForm(true)}
          title={t('discardPile.action.edit')}
          color={color}
        />
      </CardWrapper>

      {values.length > 0 && (
        <>
          {values.map((value: Value, index: number) => (
            <Box key={index}>{value}</Box>
          ))}
        </>
      )}

      {!readonly && (
        <>
          <Box className={styles.actionMenu}>
            <ActionMenu
              opened={isHovered}
              vertical={false}
              outside={false}
              opposite
              floating={false}
            >
              <RemoveButton
                onClick={() => removeColor(color)}
                title={t('discardPile.action.remove')}
              />
            </ActionMenu>
          </Box>

          {displayForm && (
            <DiscardPileEdit
              color={color}
              colorSet={colorSet}
              values={values}
              onChange={(newColor, values) => updateColorAndValues(color, newColor, values)}
              onBlur={() => setDisplayForm(false)}
            />
          )}
        </>
      )}
    </Stack>
  );
};
