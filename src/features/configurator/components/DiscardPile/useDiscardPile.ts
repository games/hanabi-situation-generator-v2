import { useAppDispatch } from '@/app/store';
import { colorsByColorSet } from '@/features/settings/helpers/color-mapping';
import { selectSettings } from '@/features/settings/slices/settingSlice.ts';
import {
  addColor,
  removeColor,
  selectDiscardPile,
  updateColor,
  updateColorAndValues,
  updateColorValues,
} from '@/features/configurator/slices/discardPileSlice';
import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';
import { useSelector } from 'react-redux';

export const useDiscardPile = () => {
  const dispatch = useAppDispatch();

  const discardPile = useSelector(selectDiscardPile);
  const { colorSet } = useSelector(selectSettings);

  const nbOfColors = colorsByColorSet(colorSet).length;

  return {
    discardPile,
    nbOfColorsInDiscardPile: Object.keys(discardPile).length,
    nbOfColors,
    addColor: () => dispatch(addColor(colorSet)),
    updateColorValues: (color: Color, values: Value[]) =>
      dispatch(
        updateColorValues({
          color,
          values,
        }),
      ),
    updateColor: (color: Color, newColor: Color) =>
      dispatch(
        updateColor({
          color,
          newColor,
        }),
      ),
    updateColorAndValues: (color: Color, newColor: Color, values: Value[]) =>
      dispatch(updateColorAndValues({ color, newColor, values })),
    removeColor: (color: Color) => dispatch(removeColor(color)),
  };
};
