import { selectSettings } from '@/features/settings/slices/settingSlice.ts';
import { FireworksEdit } from '@/features/configurator/components/Fireworks/FireworksEdit/FireworksEdit';
import { CardSimple } from '@/features/situation/components/CardSimple/CardSimple';
import { CardWrapper } from '@/features/situation/components/CardWrapper/CardWrapper';
import { Firework as FireworkModel } from '@/features/situation/model/firework/Firework.interface';
import { ActionMenu } from '@/shared/components/ActionMenu/ActionMenu';
import { RemoveButton } from '@/shared/components/buttons/RemoveButton/RemoveButton';
import { Box } from '@mui/material';
import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useHover } from 'usehooks-ts';
import styles from './firework.module.css';
import { FormClosureMode } from '@/features/settings/model/setting/FormClosureMode.enum.ts';

type Props = {
  firework: FireworkModel;
  onChange: (updated: FireworkModel) => void;
  onRemove: () => void;
  readonly?: boolean;
};

export const Firework = ({ firework, onChange, onRemove, readonly = false }: Props) => {
  const { t } = useTranslation();
  const [displayForm, setDisplayForm] = useState(false);

  const container = useRef(null);
  const isHovered = useHover(container);

  const { colorSet, formClosureMode } = useSelector(selectSettings);
  const autoHideFormAfterChange = formClosureMode === FormClosureMode.ON_CHANGE;

  return (
    <Box ref={container} className={styles.container}>
      <CardWrapper>
        <CardSimple
          color={firework.color}
          value={firework.value}
          onClick={readonly ? undefined : () => setDisplayForm(true)}
          title={t('fireworks.action.edit')}
          valueReplacement="-"
        />
      </CardWrapper>

      {!readonly && (
        <>
          <ActionMenu opened={isHovered} vertical={false} outside={false}>
            <RemoveButton onClick={onRemove} title={t('fireworks.action.remove')} />
          </ActionMenu>

          {displayForm && (
            <FireworksEdit
              firework={firework}
              colorSet={colorSet}
              onChange={(updated) => {
                onChange(updated);
                autoHideFormAfterChange && setDisplayForm(false);
              }}
              onBlur={() => setDisplayForm(false)}
            />
          )}
        </>
      )}
    </Box>
  );
};
