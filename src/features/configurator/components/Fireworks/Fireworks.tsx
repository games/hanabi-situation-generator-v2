import { Firework } from '@/features/configurator/components/Fireworks/Firework/Firework';
import { useFireworks } from '@/features/configurator/components/Fireworks/useFireworks';
import { CardWrapper } from '@/features/situation/components/CardWrapper/CardWrapper';
import { EmptySlot } from '@/features/situation/components/EmptySlot/EmptySlot';
import { Firework as FireworkModel } from '@/features/situation/model/firework/Firework.interface';
import { Button, Stack } from '@mui/material';
import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { MdAdd } from 'react-icons/md';
import styles from './fireworks.module.css';
import { useSelector } from 'react-redux';
import { selectPlayers } from '@/features/configurator/slices/playersSlice.ts';
import { removeDuplicated } from '@/shared/helpers/collection.ts';
import { Color } from '@/features/situation/model/color/Color.enum.ts';
import { Player } from '@/features/situation/model/player/Player.interface.ts';
import { selectDiscardPile } from '@/features/configurator/slices/discardPileSlice.ts';
import { HandService } from '@/features/configurator/services/HandService.ts';
import { ColorService } from '@/features/configurator/services/ColorService.ts';
import { initFireworks } from '@/features/configurator/slices/fireworksSlice.ts';
import { useAppDispatch } from '@/app/store.ts';

type Props = {
  readonly?: boolean;
};

export const Fireworks = ({ readonly = false }: Props) => {
  const { t } = useTranslation();
  const { fireworks, nbOfColors, addFirework, updateFirework, removeFirework } = useFireworks();

  const fireworkReferences = useRef([]);
  fireworkReferences.current = (fireworks ?? []).map((_item, index) => {
    return fireworkReferences.current[index] || React.createRef();
  });

  const displayFireworks = !readonly || fireworks !== undefined;

  return (
    <>
      {displayFireworks && (
        <Stack className={`${styles.container} flexBox`} spacing={1}>
          <Stack direction="row" spacing={1} alignItems="center">
            {!readonly && (
              <>
                <span>{t('fireworks.label')}</span>
                <InitFireworksButton />
              </>
            )}
          </Stack>

          <Stack direction="row" spacing={0.5}>
            {fireworks &&
              fireworks.map((firework: FireworkModel, index: number) => (
                <Firework
                  key={index}
                  firework={firework}
                  onChange={(updated: FireworkModel) => updateFirework(index, updated)}
                  onRemove={() => removeFirework(index)}
                  readonly={readonly}
                />
              ))}

            {!readonly && (fireworks?.length ?? 0) < nbOfColors && (
              <CardWrapper>
                <EmptySlot
                  onClick={() => addFirework()}
                  title={t('fireworks.action.add')}
                  emptyIcon={fireworks?.length ? <MdAdd size={'70%'} /> : undefined}
                />
              </CardWrapper>
            )}
          </Stack>
        </Stack>
      )}
    </>
  );
};

const InitFireworksButton = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const { allColorsInHand } = HandService();
  const { sortColors } = ColorService();

  const discardPile = useSelector(selectDiscardPile);
  const players = useSelector(selectPlayers);
  const usedColors = sortColors(
    removeDuplicated([
      ...(Object.keys(discardPile) as Color[]),
      ...(players ? players.flatMap(({ hand }: Player) => allColorsInHand(hand)) : []),
    ]),
  );

  return (
    <Button
      onClick={() => dispatch(initFireworks(usedColors.map((color) => ({ color }))))}
      variant="outlined"
      size="small"
    >
      {t('fireworks.action.update')}
    </Button>
  );
};
