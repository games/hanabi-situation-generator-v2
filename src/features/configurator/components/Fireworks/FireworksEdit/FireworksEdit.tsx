import { colorsByColorSet } from '@/features/settings/helpers/color-mapping';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { ColorField } from '@/features/situation/components/fields/ColorField/ColorField';
import { ValueField } from '@/features/situation/components/fields/ValueField/ValueField';
import { Color } from '@/features/situation/model/color/Color.enum';
import { Firework } from '@/features/situation/model/firework/Firework.interface';
import { ALL_VALUES } from '@/features/situation/model/value/AllValues.const';
import { Value } from '@/features/situation/model/value/Value.enum';
import { FloatingForm } from '@/shared/components/FloatingForm/FloatingForm';
import { Stack } from '@mui/material';
import { useTranslation } from 'react-i18next';
import styles from './fireworksEdit.module.css';

type Props = {
  firework: Firework;
  colorSet: ColorSet;
  onChange?: (firework: Firework) => void;
  onBlur?: () => void;
};

export const FireworksEdit = ({ firework, colorSet, onChange, onBlur }: Props) => {
  const { t } = useTranslation();

  return (
    <FloatingForm
      onBlurOrDismiss={() => {
        onBlur && onBlur();
      }}
    >
      <Stack className={styles.container} spacing={1}>
        <ValueField
          label={t('value.label')}
          values={ALL_VALUES}
          selected={firework.value}
          onChange={(value: Value | undefined) => {
            onChange && onChange({ ...firework, value });
          }}
        />
        <ColorField
          label={t('color.label')}
          colors={colorsByColorSet(colorSet)}
          selected={firework.color}
          onChange={(color: Color | undefined) => {
            onChange && onChange({ ...firework, color });
          }}
          allowUndefinedColor={false}
        />
      </Stack>
    </FloatingForm>
  );
};
