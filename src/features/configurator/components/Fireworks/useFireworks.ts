import { useAppDispatch } from '@/app/store';
import { colorsByColorSet } from '@/features/settings/helpers/color-mapping';
import { selectSettings } from '@/features/settings/slices/settingSlice.ts';
import {
  addFirework,
  removeFirework,
  selectFireworks,
  updateFirework,
} from '@/features/configurator/slices/fireworksSlice';
import { Firework } from '@/features/situation/model/firework/Firework.interface';
import { useSelector } from 'react-redux';

export const useFireworks = () => {
  const dispatch = useAppDispatch();

  const fireworks = useSelector(selectFireworks);
  const { colorSet } = useSelector(selectSettings);

  return {
    fireworks,
    nbOfColors: colorsByColorSet(colorSet).length,
    addFirework: () => dispatch(addFirework(colorSet)),
    removeFirework: (index: number) => dispatch(removeFirework(index)),
    updateFirework: (index: number, firework: Firework) => {
      dispatch(updateFirework({ index, firework }));
    },
  };
};
