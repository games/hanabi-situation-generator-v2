import { selectSettings } from '@/features/settings/slices/settingSlice.ts';
import { CardEdit } from '@/features/configurator/components/Players/CardEdit/CardEdit';
import { ClueEdit } from '@/features/configurator/components/Players/ClueEdit/ClueEdit';
import { CardWithClues } from '@/features/situation/components/CardWithClue/CardWithClues';
import { Card as CardModel } from '@/features/situation/model/card/Card.interface';
import { Clue } from '@/features/situation/model/clue/Clue.interface';
import { ActionMenu } from '@/shared/components/ActionMenu/ActionMenu';
import { RemoveButton } from '@/shared/components/buttons/RemoveButton/RemoveButton';
import { ResetButton } from '@/shared/components/buttons/ResetButton/ResetButton';
import { Box } from '@mui/material';
import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useHover } from 'usehooks-ts';
import styles from './card.module.css';
import { FormClosureMode } from '@/features/settings/model/setting/FormClosureMode.enum.ts';
import { MoveButton } from '@/shared/components/buttons/MoveButton/MoveButton.tsx';
import { useCard } from '@/features/configurator/components/Players/Card/useCard.ts';

type Props = {
  playerId: string;
  card: CardModel;
  onChange: (updated: CardModel) => void;
  onRemove: () => void;
  onReset: () => void;
  readonly?: boolean;
  onMoveTo: (cardId: string, newIndex: number) => void;
  getCardIndex: (id: string) => number;
};

export const Card = ({
  playerId,
  card,
  onChange,
  onRemove,
  onReset,
  readonly = false,
  onMoveTo,
  getCardIndex,
}: Props) => {
  const { t } = useTranslation();
  const { drag, drop } = useCard(playerId, card.id, onMoveTo, getCardIndex);
  const container = useRef(null);
  const isHovered = useHover(container);

  const [displayCardForm, setDisplayCardForm] = useState(false);
  const [displayClue1Form, setDisplayClue1Form] = useState(false);
  const [displayClue2Form, setDisplayClue2Form] = useState(false);

  const { colorSet, formClosureMode } = useSelector(selectSettings);
  const autoHideFormAfterChange = formClosureMode === FormClosureMode.ON_CHANGE;

  return (
    <>
      <Box className={styles.container}>
        <div ref={container}>
          <div ref={(node) => drag(drop(node))}>
            <CardWithClues
              card={card}
              isHovered={isHovered}
              onCardClick={readonly ? undefined : () => setDisplayCardForm(true)}
              cardTitle={t('card.action.edit')}
              onClueClick={
                readonly
                  ? undefined
                  : (index: number) => {
                      if (index === 0) {
                        setDisplayClue1Form(true);
                      } else if (index === 1) {
                        setDisplayClue2Form(true);
                      }
                    }
              }
              clue1Title={t(`clue.action.${card.clue1 ? 'edit' : 'add'}`)}
              clue2Title={t(`clue.action.${card.clue2 ? 'edit' : 'add'}`)}
              readonly={readonly}
            />
          </div>

          {!readonly && (
            <>
              <ActionMenu opened={isHovered} vertical={false} outside={false}>
                <MoveButton
                  title={t('card.action.moveLeft')}
                  onClick={() => onMoveTo(card.id, getCardIndex(card.id) - 1)}
                  direction="left"
                />
                <MoveButton
                  title={t('card.action.moveRight')}
                  onClick={() => onMoveTo(card.id, getCardIndex(card.id) + 1)}
                  direction="right"
                />
                <RemoveButton onClick={onRemove} title={t('card.action.remove')} />
                <ResetButton onClick={onReset} title={t('card.action.reset')} />
              </ActionMenu>
            </>
          )}
        </div>

        {!readonly && (
          <>
            {displayCardForm && (
              <CardEdit
                card={card}
                onChange={(updated) => {
                  onChange(updated);
                  autoHideFormAfterChange && setDisplayCardForm(false);
                }}
                onBlur={() => setDisplayCardForm(false)}
                colorSet={colorSet}
              />
            )}

            {displayClue1Form && (
              <ClueEdit
                clue={card.clue1 ?? {}}
                onChange={(clue: Clue | undefined) => {
                  onChange({ ...card, clue1: clue });
                }}
                onBlur={() => {
                  setDisplayClue1Form(false);
                }}
                colorSet={colorSet}
              />
            )}

            {displayClue2Form && (
              <ClueEdit
                clue={card.clue2 ?? {}}
                onChange={(clue: Clue | undefined) => {
                  onChange({ ...card, clue2: clue });
                }}
                onBlur={() => setDisplayClue2Form(false)}
                colorSet={colorSet}
              />
            )}
          </>
        )}
      </Box>
    </>
  );
};
