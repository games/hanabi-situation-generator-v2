import { useDrag, useDrop } from 'react-dnd';
import { DraggableType } from '@/features/configurator/model/draggable/Draggable.enum.ts';

export const useCard = (
  playerId: string,
  cardId: string,
  onMoveTo: (cardId: string, newIndex: number) => void,
  getCardIndex: (id: string) => number,
) => {
  const originalIndex = getCardIndex(cardId);
  const [, drop] = useDrop<{ id: string }, never, { canDropIn: boolean }>(
    () => ({
      accept: `${DraggableType.CARD}-${playerId}`,
      collect: (monitor) => ({
        canDropIn: monitor.isOver() && monitor.canDrop(),
      }),
      hover({ id: idOfDraggedCard }) {
        const newIndex = getCardIndex(cardId);
        if (idOfDraggedCard === cardId) {
          return;
        }

        onMoveTo(idOfDraggedCard, newIndex);
      },
    }),
    [getCardIndex, onMoveTo],
  );

  const [, drag] = useDrag(
    () => ({
      type: `${DraggableType.CARD}-${playerId}`,
      item: { id: cardId, originalIndex },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
      end: (droppedItem, monitor) => {
        const didDrop = monitor.didDrop();
        if (didDrop) {
          return;
        }

        const { id: idOfDroppedCard, originalIndex } = droppedItem;
        onMoveTo(idOfDroppedCard, originalIndex);
      },
    }),
    [cardId, originalIndex, onMoveTo],
  );

  return { drop, drag };
};
