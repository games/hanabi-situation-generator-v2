import { colorsByColorSet } from '@/features/settings/helpers/color-mapping';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { ColorField } from '@/features/situation/components/fields/ColorField/ColorField';
import { ValueField } from '@/features/situation/components/fields/ValueField/ValueField';
import { Card, Card as CardModel } from '@/features/situation/model/card/Card.interface';
import { Color } from '@/features/situation/model/color/Color.enum';
import { ALL_VALUES } from '@/features/situation/model/value/AllValues.const';
import { Value } from '@/features/situation/model/value/Value.enum';
import { FloatingForm } from '@/shared/components/FloatingForm/FloatingForm';
import { Box, Stack } from '@mui/material';
import { useTranslation } from 'react-i18next';
import styles from './cardEdit.module.css';

type Props = {
  card: CardModel;
  colorSet: ColorSet;
  onChange?: (card: Card) => void;
  onBlur?: () => void;
};

export const CardEdit = ({ card, colorSet, onChange, onBlur }: Props) => {
  const { t } = useTranslation();

  return (
    <FloatingForm
      onBlurOrDismiss={() => {
        onBlur && onBlur();
      }}
      position="left"
    >
      <Stack className={styles.container} spacing={0.5}>
        <Box className={styles.fieldset__label} sx={{ marginTop: '10rem' }}>
          {t('card.knownLabel')}
        </Box>
        <ValueField
          label={t('value.label')}
          values={ALL_VALUES}
          selected={card.knownValue}
          onChange={(value: Value | undefined) => {
            onChange && onChange({ ...card, knownValue: value });
          }}
          compact
        />
        <ColorField
          label={t('color.label')}
          colors={colorsByColorSet(colorSet)}
          selected={card.knownColor}
          onChange={(color: Color | undefined) => {
            onChange && onChange({ ...card, knownColor: color });
          }}
          compact
        />

        <Box className={styles.fieldset__label} sx={{ marginTop: '10rem' }}>
          {t('card.realDataLabel')}
        </Box>
        <ValueField
          label={t('value.label')}
          values={ALL_VALUES}
          selected={card.realValue}
          onChange={(value: Value | undefined) => {
            onChange && onChange({ ...card, realValue: value });
          }}
          compact
        />
        <ColorField
          label={t('color.label')}
          colors={colorsByColorSet(colorSet)}
          selected={card.realColor}
          onChange={(color: Color | undefined) => {
            onChange && onChange({ ...card, realColor: color });
          }}
          compact
        />
      </Stack>
    </FloatingForm>
  );
};
