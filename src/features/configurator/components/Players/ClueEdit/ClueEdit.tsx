import { colorsByColorSet } from '@/features/settings/helpers/color-mapping';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { ColorField } from '@/features/situation/components/fields/ColorField/ColorField';
import { NumberField } from '@/features/situation/components/fields/NumberField/NumberField';
import { ValueField } from '@/features/situation/components/fields/ValueField/ValueField';
import { Clue } from '@/features/situation/model/clue/Clue.interface';
import { Color } from '@/features/situation/model/color/Color.enum';
import { ALL_VALUES } from '@/features/situation/model/value/AllValues.const';
import { Value } from '@/features/situation/model/value/Value.enum';
import { FloatingForm } from '@/shared/components/FloatingForm/FloatingForm';
import { Stack } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import styles from './clueEdit.module.css';

type Props = {
  clue: Clue;
  colorSet: ColorSet;
  onChange?: (clue: Clue | undefined) => void;
  onBlur?: () => void;
};

export const ClueEdit = ({ clue, colorSet, onChange, onBlur }: Props) => {
  const { t } = useTranslation();

  return (
    <FloatingForm onBlurOrDismiss={() => onBlur && onBlur()}>
      <Stack className={styles.container} spacing={1}>
        <div>{t('clue.label')}</div>

        <ValueField
          label={t('value.label')}
          values={ALL_VALUES}
          selected={clue.value}
          onChange={(value: Value | undefined) => {
            onChange &&
              onChange(
                value
                  ? {
                      ...clue,
                      value,
                      color: undefined,
                    }
                  : undefined,
              );
            onBlur && onBlur();
          }}
          compact
        />

        <ColorField
          label={t('color.label')}
          colors={colorsByColorSet(colorSet)}
          selected={clue.color}
          onChange={(color: Color | undefined) => {
            onChange &&
              onChange(
                color
                  ? {
                      ...clue,
                      color,
                      value: undefined,
                    }
                  : undefined,
              );
            onBlur && onBlur();
          }}
          compact
        />

        <NumberField
          label={t('clue.labelNumber')}
          value={clue.number}
          onChange={(value: number | undefined) => {
            onChange && onChange({ ...clue, number: value });
          }}
          min={1}
          max={9}
        />
      </Stack>
    </FloatingForm>
  );
};
