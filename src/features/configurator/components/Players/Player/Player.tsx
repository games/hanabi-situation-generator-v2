import { selectSettings } from '@/features/settings/slices/settingSlice.ts';
import { CardWrapper } from '@/features/situation/components/CardWrapper/CardWrapper';
import { EmptySlot } from '@/features/situation/components/EmptySlot/EmptySlot';
import { Card as CardModel } from '@/features/situation/model/card/Card.interface';
import { MAX_HAND_SIZE } from '@/features/situation/model/hand/MaxHandSize.const';
import { Player as PlayerModel } from '@/features/situation/model/player/Player.interface';
import { MoveButton } from '@/shared/components/buttons/MoveButton/MoveButton';
import { RemoveButton } from '@/shared/components/buttons/RemoveButton/RemoveButton';
import { ResetButton } from '@/shared/components/buttons/ResetButton/ResetButton';
import { Box, Button, Stack } from '@mui/material';
import { blue } from '@mui/material/colors';
import MuiInput from '@mui/material/Input';
import React, { BaseSyntheticEvent, ReactNode, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { MdAdd, MdEdit } from 'react-icons/md';
import { useSelector } from 'react-redux';
import styles from './player.module.css';
import { useHover } from 'usehooks-ts';

type Props = {
  children: ReactNode;
  player: PlayerModel;
  onRename: (name: string) => void;
  onRemove: () => void;
  onMoveUp: () => void;
  onMoveDown: () => void;
  onResetHand: () => void;
  onUpdateHand: (hand: CardModel[]) => void;
  onAddCard: () => void;
  readonly?: boolean;
};

export const Player = ({
  children,
  player,
  onRename,
  onRemove,
  onMoveUp,
  onMoveDown,
  onResetHand,
  onAddCard,
  readonly = false,
}: Props) => {
  const { t } = useTranslation();

  const [displayNameForm, setDisplayNameForm] = useState(false);

  const container = useRef(null);
  const isHovered = useHover(container);
  const { cardDrawDirection } = useSelector(selectSettings);

  return (
    <div>
      <Stack
        className={styles.container}
        sx={{ backgroundColor: !readonly && isHovered ? blue[50] : undefined }}
        direction="row"
        spacing={2}
        ref={container}
        alignItems={!readonly ? 'flex-start' : undefined}
      >
        <Stack className={styles.player} direction="column" spacing={2}>
          {displayNameForm ? (
            <MuiInput
              className={styles.nameInput}
              value={player.name}
              onChange={(event: BaseSyntheticEvent) => {
                onRename(event.target.value);
              }}
              onBlur={() => setDisplayNameForm(false)}
            />
          ) : (
            <>
              <Stack>
                {readonly ? (
                  <>{player.name}</>
                ) : (
                  <Button
                    className={styles.renameButton}
                    onClick={readonly ? undefined : () => setDisplayNameForm(true)}
                    title={t('players.action.rename')}
                    variant="text"
                    sx={{
                      [`&`]: { color: 'black', textTransform: 'initial' },
                    }}
                  >
                    <MdEdit className={styles.renameButton__icon} />
                    {player.name}
                  </Button>
                )}

                <Box className={styles.arrowContainer}>
                  <Box
                    className={`${styles.arrow} ${styles[cardDrawDirection]}`}
                    title={t('players.arrow')}
                  ></Box>
                </Box>

                {isHovered && !readonly && (
                  <>
                    <Stack direction="row">
                      <RemoveButton title={t('players.action.remove')} onClick={onRemove} />
                      <ResetButton title={t('players.action.reset')} onClick={onResetHand} />
                    </Stack>
                    <Stack direction="row">
                      <MoveButton
                        title={t('players.action.moveUp')}
                        onClick={onMoveUp}
                        direction="top"
                      />
                      <MoveButton
                        title={t('players.action.moveDown')}
                        onClick={onMoveDown}
                        direction="bottom"
                      />
                    </Stack>
                  </>
                )}
              </Stack>
            </>
          )}
        </Stack>

        <Stack className={styles.hand} direction="row" spacing={0.5}>
          {children}

          {!readonly && player.hand.length < MAX_HAND_SIZE && (
            <CardWrapper canHaveClue>
              <EmptySlot
                onClick={onAddCard}
                title={t('card.action.add')}
                emptyIcon={player.hand.length ? <MdAdd size={'70%'} /> : undefined}
              />
            </CardWrapper>
          )}
        </Stack>
      </Stack>
    </div>
  );
};
