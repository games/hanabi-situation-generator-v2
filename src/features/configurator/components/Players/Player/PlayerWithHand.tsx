import { Card } from '@/features/configurator/components/Players/Card/Card';
import { Card as CardModel } from '@/features/situation/model/card/Card.interface';
import React, { useCallback } from 'react';
import { HandService } from '@/features/configurator/services/HandService.ts';
import { Player } from '@/features/configurator/components/Players/Player/Player.tsx';
import { selectPlayerById } from '@/features/configurator/slices/playersSlice.ts';
import { useAppSelector } from '@/app/store.ts';
import { usePlayers } from '@/features/configurator/components/Players/usePlayers.ts';

type Props = {
  playerId: string;
  playerIndex: number;
  previousPlayerIndex: number;
  nextPlayerIndex: number;
  readonly?: boolean;
};

export const PlayerWithHand = ({
  playerId,
  playerIndex,
  previousPlayerIndex,
  nextPlayerIndex,
  readonly = false,
}: Props) => {
  const { moveCard } = HandService();
  const player = useAppSelector((state) => selectPlayerById(state, playerId));
  if (!player) {
    return <></>;
  }

  const {
    renamePlayer,
    removePlayer,
    movePlayer,
    resetPlayerHand,
    addCardToHand,
    updateHand,
    removeCardInHand,
    resetCardInHand,
    updateCardInHand,
  } = usePlayers();

  const getCardIndex = useCallback(
    (id: string) => player.hand.findIndex((card: CardModel) => card.id === id),
    [player],
  );
  const onMoveCardTo = useCallback(
    (cardId: string, newIndex: number) => {
      let fixedNewIndex = newIndex >= 0 ? newIndex : player.hand.length - 1;
      if (fixedNewIndex > player.hand.length - 1) {
        fixedNewIndex = 0;
      }

      updateHand(playerIndex, moveCard(player.hand, getCardIndex(cardId), fixedNewIndex));
    },
    [moveCard, getCardIndex],
  );

  return (
    <Player
      player={player}
      onRename={(name) => renamePlayer(playerIndex, name)}
      onRemove={() => removePlayer(playerIndex)}
      onMoveDown={() => movePlayer(playerIndex, nextPlayerIndex)}
      onMoveUp={() => movePlayer(playerIndex, previousPlayerIndex)}
      onResetHand={() => resetPlayerHand(playerIndex)}
      onUpdateHand={(hand: CardModel[]) => updateHand(playerIndex, hand)}
      onAddCard={() => addCardToHand(playerIndex)}
      readonly={readonly}
    >
      <>
        {player.hand.map((card: CardModel, index: number) => (
          <Card
            key={card.id}
            playerId={player.id}
            card={card}
            onChange={(updatedCard) => updateCardInHand(playerIndex, index, updatedCard)}
            onRemove={() => removeCardInHand(playerIndex, index)}
            onReset={() => resetCardInHand(playerIndex, index)}
            readonly={readonly}
            onMoveTo={onMoveCardTo}
            getCardIndex={getCardIndex}
          />
        ))}
      </>
    </Player>
  );
};
