import { usePlayers } from '@/features/configurator/components/Players/usePlayers';
import { EmptySlot } from '@/features/situation/components/EmptySlot/EmptySlot';
import { MAX_NB_OF_PLAYERS } from '@/features/situation/model/player/MaxNbOfPlayers.const';
import { Box, Button, IconButton, Stack } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { MdAddToPhotos, MdPersonAddAlt1 } from 'react-icons/md';
import styles from './players.module.css';
import { PlayerWithHand } from '@/features/configurator/components/Players/Player/PlayerWithHand.tsx';
import { useSelector } from 'react-redux';
import { selectPlayerIds } from '@/features/configurator/slices/playersSlice.ts';

type Props = {
  readonly?: boolean;
};

const PlayerWithHandMemo = React.memo(PlayerWithHand);

export const Players = ({ readonly = false }: Props) => {
  const { t } = useTranslation();
  const { initNbOfPlayers, addPlayer, increaseHandSize } = usePlayers();
  const playerIds = useSelector(selectPlayerIds) ?? [];
  const nbOfPlayers = playerIds.length;

  const displayPlayers = !readonly || nbOfPlayers > 0;
  const isFirstPLayer = (index: number) => index === 0;
  const isLastPLayer = (index: number) => index === nbOfPlayers - 1;

  return (
    <>
      {displayPlayers && (
        <Stack className={`${styles.container}`}>
          {!readonly && (
            <>
              <Box className={styles.label}>{t('players.label')}</Box>

              <Box className={styles.buttons}>
                <Button
                  onClick={() => initNbOfPlayers(3)}
                  variant="outlined"
                  size="small"
                  sx={{ mx: 0.5 }}
                >
                  {t('players.action.init3Players')}
                </Button>
                <Button
                  onClick={() => initNbOfPlayers(4)}
                  variant="outlined"
                  size="small"
                  sx={{ mx: 0.5 }}
                >
                  {t('players.action.init4Players')}
                </Button>
                {nbOfPlayers > 0 && (
                  <IconButton
                    onClick={increaseHandSize}
                    title={t('players.action.increaseHandSize')}
                    sx={{ mx: 0.5 }}
                  >
                    <MdAddToPhotos />
                  </IconButton>
                )}
              </Box>
            </>
          )}

          {playerIds.map((id: string, index: number) => (
            <PlayerWithHandMemo
              key={id}
              playerId={id}
              playerIndex={index}
              previousPlayerIndex={isFirstPLayer(index) ? nbOfPlayers - 1 : index - 1}
              nextPlayerIndex={isLastPLayer(index) ? 0 : index + 1}
              readonly={readonly}
            />
          ))}

          {!readonly && nbOfPlayers < MAX_NB_OF_PLAYERS && (
            <Box sx={{ height: '2.5rem', width: '100%' }}>
              <EmptySlot
                onClick={() => addPlayer()}
                title={t('players.action.add')}
                emptyIcon={nbOfPlayers > 0 ? <MdPersonAddAlt1 size={'70%'} /> : undefined}
                addIcon={<MdPersonAddAlt1 size={'70%'} />}
              />
            </Box>
          )}
        </Stack>
      )}
    </>
  );
};
