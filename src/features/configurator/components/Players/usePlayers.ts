import { useAppDispatch } from '@/app/store';
import {
  addCardToHand,
  addPlayer,
  increaseHandSize,
  initNbOfPlayers,
  movePlayer,
  removeCardInHand,
  removePlayer,
  renamePlayer,
  resetCardInHand,
  resetPlayerHand,
  updateCardInHand,
  updateHand,
} from '@/features/configurator/slices/playersSlice';
import { Card } from '@/features/situation/model/card/Card.interface';

export const usePlayers = () => {
  const dispatch = useAppDispatch();

  return {
    initNbOfPlayers: (nb: number) => dispatch(initNbOfPlayers(nb)),
    addPlayer: () => dispatch(addPlayer()),
    removePlayer: (index: number) => dispatch(removePlayer(index)),
    movePlayer: (currentIndex: number, newIndex: number) => {
      return dispatch(movePlayer({ currentIndex, newIndex }));
    },
    resetPlayerHand: (index: number) => dispatch(resetPlayerHand(index)),
    renamePlayer: (index: number, name: string) => {
      return dispatch(renamePlayer({ index, name }));
    },
    addCardToHand: (indexOfPlayer: number) => {
      return dispatch(addCardToHand(indexOfPlayer));
    },
    updateHand: (indexOfPlayer: number, hand: Card[]) => {
      return dispatch(updateHand({ indexOfPlayer, hand }));
    },
    updateCardInHand: (indexOfPlayer: number, indexOfCard: number, card: Card) => {
      return dispatch(updateCardInHand({ indexOfPlayer, indexOfCard, card }));
    },
    removeCardInHand: (indexOfPlayer: number, indexOfCard: number) => {
      return dispatch(removeCardInHand({ indexOfPlayer, indexOfCard }));
    },
    resetCardInHand: (indexOfPlayer: number, indexOfCard: number) => {
      return dispatch(resetCardInHand({ indexOfPlayer, indexOfCard }));
    },
    increaseHandSize: () => {
      return dispatch(increaseHandSize());
    },
  };
};
