export enum DraggableType {
  PLAYER = 'player',
  CARD = 'card',
}
