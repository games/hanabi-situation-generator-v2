import { Color } from '@/features/situation/model/color/Color.enum.ts';
import { ALL_COLORS } from '@/features/situation/model/color/AllColors.const.ts';

export const ColorService = () => {
  return {
    sortColors,
  };
};

const sortColors = (colors: Color[]) => {
  return ALL_COLORS.reduce((sortedColors, color) => {
    return [...sortedColors, ...(colors.includes(color) ? [color] : [])];
  }, [] as Color[]);
};
