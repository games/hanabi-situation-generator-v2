import { colorsByColorSet } from '@/features/settings/helpers/color-mapping';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { Color } from '@/features/situation/model/color/Color.enum';
import { DiscardPile } from '@/features/situation/model/discardPile/DiscardPile.interface';
import { Value } from '@/features/situation/model/value/Value.enum';

export const DiscardPileService = () => {
  return {
    addColor,
    updateColorValues,
    updateColor,
    updateColorAndValues,
    removeColor,
  };
};

const getNextColor = (discardPile: DiscardPile, colors: Color[]): Color | undefined => {
  let color = undefined;
  for (let i = 0; i < colors.length; i++) {
    color = colors[i];
    if (!(color in discardPile)) {
      break;
    }
  }
  return color;
};

const addColor = (discardPile: DiscardPile, colorSet: ColorSet): DiscardPile => {
  const color = getNextColor(discardPile, colorsByColorSet(colorSet));

  return {
    ...discardPile,
    ...(color ? { [color]: [] } : {}),
  };
};

const updateColorValues = (
  discardPile: DiscardPile,
  color: Color,
  values: Value[],
): DiscardPile => {
  return {
    ...discardPile,
    [color]: [values],
  };
};

const updateColorAndValues = (
  discardPile: DiscardPile,
  color: Color,
  newColor: Color,
  values: Value[],
): DiscardPile => {
  const updatedDiscardPile = {
    ...discardPile,
    [newColor]: color === newColor ? values : [...(discardPile[newColor] ?? []), ...values],
  };
  if (color !== newColor) {
    delete updatedDiscardPile[color];
  }

  return updatedDiscardPile;
};

const updateColor = (discardPile: DiscardPile, color: Color, newColor: Color): DiscardPile => {
  const updatedDiscardPile = {
    ...discardPile,
    [newColor]: [...(discardPile[color] ?? []), ...(discardPile[newColor] ?? [])],
  };
  delete discardPile[color];

  return updatedDiscardPile;
};

const removeColor = (discardPile: DiscardPile, color: Color): DiscardPile => {
  const updatedDiscardPile = {
    ...discardPile,
  };
  delete updatedDiscardPile[color];

  return updatedDiscardPile;
};
