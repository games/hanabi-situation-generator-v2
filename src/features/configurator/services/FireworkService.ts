import { colorsByColorSet } from '@/features/settings/helpers/color-mapping';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { Color } from '@/features/situation/model/color/Color.enum';
import { Firework } from '@/features/situation/model/firework/Firework.interface';
import { getItem } from '@/shared/helpers/collection';

export const FireworkService = () => {
  return {
    initFirework,
    addFirework,
    updateFirework,
    removeFirework,
  };
};

const initFirework = (): Firework => ({});

const getNextColor = (fireworks: Firework[] | undefined, colors: Color[]): Color | undefined => {
  let color: Color | undefined = undefined;
  if ((fireworks ?? []).length === colors.length) {
    return undefined;
  }

  for (let i = 0; i < colors.length; i++) {
    color = colors[i];
    const doesColorExists = (fireworks || []).some(
      (firework: Firework) => firework.color === color,
    );
    if (!doesColorExists) {
      break;
    }
  }
  return color;
};

const addFirework = (fireworks: Firework[] | undefined, colorSet: ColorSet): Firework[] => {
  const color = getNextColor(fireworks, colorsByColorSet(colorSet));

  return [
    ...(fireworks || []),
    ...(color
      ? [
          {
            ...initFirework(),
            color,
          },
        ]
      : []),
  ];
};

const updateFirework = (
  fireworks: Firework[] | undefined,
  index: number,
  firework: Firework,
): Firework[] => {
  return [...(fireworks || [])].toSpliced(index, 1, firework);
};

const removeFirework = (fireworks: Firework[] | undefined, index: number): Firework[] => {
  getItem(fireworks || [], index);
  return [...(fireworks || [])].toSpliced(index, 1);
};
