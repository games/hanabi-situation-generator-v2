import { Card } from '@/features/situation/model/card/Card.interface';
import { HandSize } from '@/features/situation/model/hand/HandSize.enum';
import { Color } from '@/features/situation/model/color/Color.enum.ts';
import { uuid } from '@/shared/helpers/id.ts';
import { getItem, move } from '@/shared/helpers/collection.ts';

export const HandService = () => {
  return {
    initHand,
    addCardToHand,
    moveCard,
    updateCard,
    removeCard,
    resetCard,
    allColorsInHand,
  };
};

const initHand = (size: HandSize): Card[] =>
  Array(size)
    .fill({})
    .map(() => ({ id: uuid() }));
const addCardToHand = (hand: Card[]): Card[] => [...hand, { id: uuid() }];

const moveCard = (hand: Card[], currentIndex: number, newIndex: number): Card[] => {
  getItem(hand, currentIndex);
  getItem(hand, newIndex);
  return move(hand, currentIndex, newIndex);
};

const updateCard = (hand: Card[] | undefined, index: number, card: Card): Card[] => {
  return [...(hand || [])].toSpliced(index, 1, card);
};

const removeCard = (hand: Card[] | undefined, index: number): Card[] => {
  return [...(hand || [])].toSpliced(index, 1);
};

const resetCard = (hand: Card[] | undefined, index: number): Card[] => {
  return updateCard(hand, index, { id: uuid() });
};

const allColorsInHand = (hand: Card[]): Color[] => {
  return Array.from(
    new Set(
      hand.reduce((colorList, { knownColor, realColor, clue1, clue2 }: Card) => {
        return [
          ...colorList,
          ...(knownColor ? [knownColor] : []),
          ...(realColor ? [realColor] : []),
          ...(clue1?.color ? [clue1?.color] : []),
          ...(clue2?.color ? [clue2?.color] : []),
        ];
      }, [] as Color[]),
    ),
  );
};
