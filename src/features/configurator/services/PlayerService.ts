import { HandService } from '@/features/configurator/services/HandService';
import { Card } from '@/features/situation/model/card/Card.interface';
import { HandSize } from '@/features/situation/model/hand/HandSize.enum';
import { MAX_HAND_SIZE } from '@/features/situation/model/hand/MaxHandSize.const';
import { ALL_PLAYER_NAMES } from '@/features/situation/model/player/AllPlayerNames.const';
import { Player } from '@/features/situation/model/player/Player.interface';
import { PlayerName } from '@/features/situation/model/player/PlayerName.enum';
import { getItem, move } from '@/shared/helpers/collection';
import { produce } from 'immer';
import { uuid } from '@/shared/helpers/id.ts';

export const PlayerService = () => {
  return {
    initPlayers,
    addPlayer,
    renamePlayer,
    removePlayer,
    movePlayer,
    resetPlayerHand,
    addCardToHand,
    updateHand,
    updateCardInHand,
    removeCardFromHand,
    resetCardFromHand,
    increaseHandSize,
  };
};
const handService = HandService();

const initPlayer = (
  handSize: HandSize,
  name: string | PlayerName | undefined = undefined,
): Player => ({
  id: uuid(),
  name: name ?? PlayerName.ALICE,
  hand: handService.initHand(handSize),
});

const initPlayers = (nb: number): Player[] => {
  if (nb < 1 || nb > 5) {
    throw new Error('Number of players must be between 1 and 5');
  }

  const handSize = {
    2: HandSize.TWO_THREE_PLAYERS,
    3: HandSize.TWO_THREE_PLAYERS,
    4: HandSize.FOUR_FIVE_PLAYERS,
    5: HandSize.FOUR_FIVE_PLAYERS,
  }[nb] as HandSize;

  let players: Player[] = [];
  for (let i = 0; i < nb; i++) {
    players = addPlayer(players, handSize);
  }

  return players;
};

const addPlayer = (players: Player[], handSize: HandSize): Player[] => {
  const name = ALL_PLAYER_NAMES[(players?.length || 0) % ALL_PLAYER_NAMES.length];

  return produce(players || [], (draft: Player[]) => {
    draft.push({
      ...initPlayer(handSize, name),
    });
  });
};

const removePlayer = (players: Player[], index: number): Player[] => {
  getItem(players, index);
  return players.toSpliced(index, 1);
};

const movePlayer = (players: Player[], currentIndex: number, newIndex: number): Player[] => {
  getItem(players, currentIndex);
  getItem(players, newIndex);
  return move(players, currentIndex, newIndex);
};

const updatePlayer = (players: Player[], index: number, player: Player): Player[] => {
  return players.toSpliced(index, 1, player);
};

const renamePlayer = (players: Player[], index: number, name: string | PlayerName): Player[] => {
  const updatedPlayer = {
    ...players[index],
    name,
  };

  return updatePlayer(players, index, updatedPlayer);
};

const resetPlayerHand = (players: Player[], index: number): Player[] => {
  const updatedPlayer = {
    ...players[index],
    hand: handService.initHand(players[index].hand.length),
  };

  return updatePlayer(players, index, updatedPlayer);
};

const addCardToHand = (players: Player[], index: number): Player[] => {
  const updatedPlayer = produce(getItem(players, index), (draft: Player) => {
    draft.hand = handService.addCardToHand(draft.hand);
  });

  return updatePlayer(players, index, updatedPlayer);
};

const increaseHandSize = (players: Player[]): Player[] => {
  let updatedPlayers = players;
  for (let index = 0; index < players.length; index++) {
    if (updatedPlayers[index].hand.length < MAX_HAND_SIZE) {
      updatedPlayers = addCardToHand(updatedPlayers, index);
    }
  }

  return updatedPlayers;
};

const updateHand = (players: Player[], indexOfPlayer: number, hand: Card[]): Player[] => {
  const updatedPlayer = produce(getItem(players, indexOfPlayer), (draft: Player) => {
    draft.hand = hand;
  });

  return updatePlayer(players, indexOfPlayer, updatedPlayer);
};

const updateCardInHand = (
  players: Player[],
  indexOfPlayer: number,
  indexOfCard: number,
  card: Card,
): Player[] => {
  const updatedPlayer = produce(getItem(players, indexOfPlayer), (draft: Player) => {
    draft.hand = handService.updateCard(draft.hand, indexOfCard, card);
  });

  return updatePlayer(players, indexOfPlayer, updatedPlayer);
};

const removeCardFromHand = (
  players: Player[],
  indexOfPlayer: number,
  indexOfCard: number,
): Player[] => {
  const updatedPlayer = produce(getItem(players, indexOfPlayer), (draft: Player) => {
    draft.hand = handService.removeCard(draft.hand, indexOfCard);
  });

  return updatePlayer(players, indexOfPlayer, updatedPlayer);
};

const resetCardFromHand = (
  players: Player[],
  indexOfPlayer: number,
  indexOfCard: number,
): Player[] => {
  const updatedPlayer = produce(getItem(players, indexOfPlayer), (draft: Player) => {
    draft.hand = handService.resetCard(draft.hand, indexOfCard);
  });

  return updatePlayer(players, indexOfPlayer, updatedPlayer);
};
