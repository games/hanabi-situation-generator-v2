import { PlayerService } from '@/features/configurator/services/PlayerService';
import { Situation } from '@/features/situation/model/situation/Situation.interface';
import { normalizeAsJsonBase64 } from '@/shared/helpers/normalization';
import { getBaseUrl } from '@/shared/helpers/url';
import { generatePath } from 'react-router-dom';
import { ShortenerService } from '@/features/url-shortener/services/ShortenerService.ts';
import { NbOfPlayers } from '@/features/situation/model/player/NbOfPlayers.enum.ts';

export const SituationService = () => {
  return {
    getDefaultSituation,
    getShareUrl,
  };
};

const { initPlayers } = PlayerService();
const { shortenUrl } = ShortenerService();

const getDefaultSituation = (nbOfPlayers: number = +NbOfPlayers.THREE): Situation => {
  return {
    players: initPlayers(nbOfPlayers),
  };
};

const getShareUrl = async (situation: Situation, shorten = false): Promise<string> => {
  const url =
    getBaseUrl() +
    generatePath('/:base64SituationData', {
      base64SituationData: normalizeAsJsonBase64(situation),
    });

  return shorten ? await shortenUrl(url) : url;
};
