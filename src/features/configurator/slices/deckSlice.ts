import { RootState } from '@/app/store';
import { Deck } from '@/features/situation/model/deck/Deck.interface';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState: { content: Deck | undefined } = {
  content: undefined,
};

const deckSlice = createSlice({
  name: 'deck',
  initialState,
  reducers: {
    setNbRemainingCards: (state, action: PayloadAction<number | undefined>) => {
      const nbRemainingCards = action.payload;
      state.content = nbRemainingCards ? { nbRemainingCards } : undefined;
    },
  },
});

export const deckReducer = deckSlice.reducer;

export const { setNbRemainingCards } = deckSlice.actions;

export const selectDeck = (state: RootState) => state.deck.content;
