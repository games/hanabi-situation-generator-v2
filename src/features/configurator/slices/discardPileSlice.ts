import { RootState } from '@/app/store';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { DiscardPileService } from '@/features/configurator/services/DiscardPileService';
import { Color } from '@/features/situation/model/color/Color.enum';
import { DiscardPile } from '@/features/situation/model/discardPile/DiscardPile.interface';
import { Value } from '@/features/situation/model/value/Value.enum';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const discardPileService = DiscardPileService();

const initialState: { pile: DiscardPile } = {
  pile: {},
};

const discardPileSlice = createSlice({
  name: 'discardPile',
  initialState,
  reducers: {
    initDiscardPile: (state, action: PayloadAction<DiscardPile | undefined>) => {
      const discardPile = action.payload;
      state.pile = { ...discardPile };
    },
    addColor: (state, action: PayloadAction<ColorSet>) => {
      const colorSet = action.payload;
      state.pile = discardPileService.addColor(state.pile, colorSet);
    },
    updateColorValues: (
      state,
      action: PayloadAction<{
        color: Color;
        values: Value[];
      }>,
    ) => {
      const { color, values } = action.payload;
      state.pile = discardPileService.updateColorValues(state.pile, color, values);
    },
    updateColor: (
      state,
      action: PayloadAction<{
        color: Color;
        newColor: Color;
      }>,
    ) => {
      const { color, newColor } = action.payload;
      state.pile = discardPileService.updateColor(state.pile, color, newColor);
    },
    updateColorAndValues: (
      state,
      action: PayloadAction<{
        color: Color;
        newColor: Color;
        values: Value[];
      }>,
    ) => {
      const { color, newColor, values } = action.payload;
      state.pile = discardPileService.updateColorAndValues(state.pile, color, newColor, values);
    },
    removeColor: (state, action: PayloadAction<Color>) => {
      const color = action.payload;
      state.pile = discardPileService.removeColor(state.pile, color);
    },
  },
});

export const discardPileReducer = discardPileSlice.reducer;

export const {
  initDiscardPile,
  addColor,
  updateColorValues,
  updateColor,
  updateColorAndValues,
  removeColor,
} = discardPileSlice.actions;

export const selectDiscardPile = (state: RootState) => state.discardPile.pile;
