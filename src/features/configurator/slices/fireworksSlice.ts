import { RootState } from '@/app/store';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { FireworkService } from '@/features/configurator/services/FireworkService';
import { Firework } from '@/features/situation/model/firework/Firework.interface';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const fireworkListService = FireworkService();

const initialState: { list: Firework[] | undefined } = {
  list: undefined,
};

const fireworksSlice = createSlice({
  name: 'fireworks',
  initialState,
  reducers: {
    initFireworks: (state, action: PayloadAction<Firework[] | undefined>) => {
      const fireworks = action.payload;
      state.list = fireworks ? [...fireworks] : undefined;
    },
    updateFirework: (
      state,
      action: PayloadAction<{
        index: number;
        firework: Firework;
      }>,
    ) => {
      const { index, firework } = action.payload;
      state.list = fireworkListService.updateFirework(state.list, index, firework);
    },
    addFirework: (state, action: PayloadAction<ColorSet>) => {
      const colorSet = action.payload;
      state.list = fireworkListService.addFirework(state.list, colorSet);
    },
    removeFirework: (state, action: PayloadAction<number>) => {
      const index = action.payload;
      state.list = fireworkListService.removeFirework(state.list, index);
      if (state.list.length === 0) {
        state.list = undefined;
      }
    },
  },
});

export const fireworksReducer = fireworksSlice.reducer;

export const { updateFirework, initFireworks, addFirework, removeFirework } =
  fireworksSlice.actions;

export const selectFireworks = (state: RootState) => state.fireworks.list;
