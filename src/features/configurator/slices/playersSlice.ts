import { RootState } from '@/app/store';
import { PlayerService } from '@/features/configurator/services/PlayerService';
import { Card } from '@/features/situation/model/card/Card.interface';
import { HandSize } from '@/features/situation/model/hand/HandSize.enum';
import { Player } from '@/features/situation/model/player/Player.interface';
import { PlayerName } from '@/features/situation/model/player/PlayerName.enum';
import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';

const playerService = PlayerService();

const initialState: { list: Player[] | undefined } = {
  list: [],
};

export const playersSlice = createSlice({
  name: 'players',
  initialState,
  reducers: {
    initPlayers: (state, action: PayloadAction<Player[] | undefined>) => {
      const players = action.payload;
      state.list = players ? [...players] : undefined;
    },
    initNbOfPlayers: (state, action: PayloadAction<number>) => {
      const nb = action.payload;
      state.list = playerService.initPlayers(nb);
    },
    addPlayer: (state, action: PayloadAction<HandSize | undefined>) => {
      const handSize =
        action.payload ||
        (state.list ? (state.list[0].hand.length as HandSize) : HandSize.FOUR_FIVE_PLAYERS);
      state.list = playerService.addPlayer(state.list ?? [], handSize);
    },
    removePlayer: (state, action: PayloadAction<number>) => {
      const index = action.payload;
      state.list = playerService.removePlayer(state.list ?? [], index);
      if (state.list.length === 0) {
        state.list = undefined;
      }
    },
    movePlayer: (
      state,
      action: PayloadAction<{
        currentIndex: number;
        newIndex: number;
      }>,
    ) => {
      const { currentIndex, newIndex } = action.payload;
      state.list = playerService.movePlayer(state.list ?? [], currentIndex, newIndex);
    },
    renamePlayer: (
      state,
      action: PayloadAction<{
        index: number;
        name: string | PlayerName;
      }>,
    ) => {
      const { index, name } = action.payload;
      state.list = playerService.renamePlayer(state.list ?? [], index, name);
    },
    resetPlayerHand: (state, action: PayloadAction<number>) => {
      const index = action.payload;
      state.list = playerService.resetPlayerHand(state.list ?? [], index);
    },
    addCardToHand: (state, action: PayloadAction<number>) => {
      const indexOfPlayer = action.payload;
      state.list = playerService.addCardToHand(state.list ?? [], indexOfPlayer);
    },
    updateHand: (state, action: PayloadAction<{ indexOfPlayer: number; hand: Card[] }>) => {
      const { indexOfPlayer, hand } = action.payload;
      state.list = playerService.updateHand(state.list ?? [], indexOfPlayer, hand);
    },
    updateCardInHand: (
      state,
      action: PayloadAction<{
        indexOfPlayer: number;
        indexOfCard: number;
        card: Card;
      }>,
    ) => {
      const { indexOfPlayer, indexOfCard, card } = action.payload;
      state.list = playerService.updateCardInHand(
        state.list ?? [],
        indexOfPlayer,
        indexOfCard,
        card,
      );
    },
    removeCardInHand: (
      state,
      action: PayloadAction<{
        indexOfPlayer: number;
        indexOfCard: number;
      }>,
    ) => {
      const { indexOfPlayer, indexOfCard } = action.payload;
      state.list = playerService.removeCardFromHand(state.list ?? [], indexOfPlayer, indexOfCard);
    },
    resetCardInHand: (
      state,
      action: PayloadAction<{
        indexOfPlayer: number;
        indexOfCard: number;
      }>,
    ) => {
      const { indexOfPlayer, indexOfCard } = action.payload;
      state.list = playerService.resetCardFromHand(state.list ?? [], indexOfPlayer, indexOfCard);
    },
    increaseHandSize: (state) => {
      if (state.list) {
        state.list = playerService.increaseHandSize(state.list);
      }
    },
  },
});

export const playersReducer = playersSlice.reducer;

export const {
  initPlayers,
  initNbOfPlayers,
  addPlayer,
  removePlayer,
  movePlayer,
  renamePlayer,
  resetPlayerHand,
  addCardToHand,
  updateHand,
  updateCardInHand,
  removeCardInHand,
  resetCardInHand,
  increaseHandSize,
} = playersSlice.actions;

export const selectPlayers = (state: RootState) => state.players.list;
export const selectPlayerIds = createSelector([selectPlayers], (players) =>
  (players ?? []).map(({ id }) => id),
);
export const selectPlayerById = (state: RootState, playerId: string) =>
  (state.players.list ?? []).find((player) => player.id === playerId);
