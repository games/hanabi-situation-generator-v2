import { ConverterService } from '@/features/url-converter/services/ConverterService';

export const useRedirectToV2 = () => {
  const { convertUrl } = ConverterService();

  if (location.search && !location.search.includes('redirected=1')) {
    convertUrl(location.toString()).then((url: string) => {
      console.log(location.toString(), url);
      location.replace(`${url}?redirected=1`);
    });
  }

  return {
    redirected: location.search.includes('redirected=1'),
  };
};
