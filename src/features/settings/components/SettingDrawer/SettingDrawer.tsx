import { useSettingDrawer } from '@/features/settings/components/SettingDrawer/useSettingDrawer.ts';
import { ALL_COLOR_SETS } from '@/features/settings/model/color/AllColorSets.const';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { ALL_CARD_DRAW_DIRECTIONS } from '@/features/settings/model/setting/AllCardDrawDirections.const';
import { CardDrawDirection } from '@/features/settings/model/setting/CardDrawDirection.enum';
import { ColorList } from '@/features/situation/components/ColorList/ColorList';
import {
  Checkbox,
  Drawer,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Stack,
} from '@mui/material';
import React, { ChangeEvent } from 'react';
import { useTranslation } from 'react-i18next';
import styles from './settingDrawer.module.css';
import { ALL_FORM_CLOSURE_MODES } from '@/features/settings/model/setting/AllFormClosureModes.const.ts';
import { FormClosureMode } from '@/features/settings/model/setting/FormClosureMode.enum.ts';
import { ALL_NB_OF_PLAYERS } from '@/features/situation/model/player/AllNbOfPlayers.const.ts';
import { NbOfPlayers } from '@/features/situation/model/player/NbOfPlayers.enum.ts';

type Props = {
  opened: boolean;
  onClose: () => void;
};

export const SettingDrawer = ({ opened, onClose }: Props) => {
  const { t } = useTranslation();
  const {
    colorSet,
    hideDeck,
    defaultNbOfPlayers,
    cardDrawDirection,
    colorsByColorSet,
    formClosureMode,
    setColorSet,
    setDefaultNbOfPlayers,
    toggleHideDeck,
    toggleCardDrawDirection,
    toggleFormClosureMode,
  } = useSettingDrawer();

  return (
    <Drawer open={opened} onClose={onClose} anchor="right">
      <Stack className={styles.innerContainer}>
        <h4>{t('settings.label')}</h4>
        <Stack className={styles.parameter} spacing={2}>
          <FormControlLabel
            control={<Checkbox color="primary" onChange={toggleHideDeck} checked={hideDeck} />}
            label={t('settings.list.hideDeck')}
          />

          <FormControl>
            <FormLabel id="setting-default-nb-of-players">
              {t('settings.list.defaultNbOfPlayers')}
            </FormLabel>
            <RadioGroup
              aria-labelledby="setting-default-nb-of-players"
              defaultValue={defaultNbOfPlayers}
              name="radio-buttons-group"
              onChange={(_event, value) => setDefaultNbOfPlayers(+value)}
              sx={{ flexDirection: 'row' }}
            >
              {ALL_NB_OF_PLAYERS.map((nbOfPlayers: NbOfPlayers, index: number) => (
                <React.Fragment key={index}>
                  <FormControlLabel value={nbOfPlayers} control={<Radio />} label={nbOfPlayers} />
                </React.Fragment>
              ))}
            </RadioGroup>
          </FormControl>

          <FormControl>
            <FormLabel id="setting-card-draw-direction">
              {t('settings.list.cardDrawDirection')}
            </FormLabel>
            <RadioGroup
              aria-labelledby="setting-card-draw-direction"
              defaultValue={cardDrawDirection}
              name="radio-buttons-group"
              onChange={() => toggleCardDrawDirection()}
            >
              {ALL_CARD_DRAW_DIRECTIONS.map(
                (cardDrawDirection: CardDrawDirection, index: number) => (
                  <React.Fragment key={index}>
                    <FormControlLabel
                      value={cardDrawDirection}
                      control={<Radio />}
                      label={t(`cardDrawDirection.${cardDrawDirection}`)}
                    />
                  </React.Fragment>
                ),
              )}
            </RadioGroup>
          </FormControl>

          <FormControl>
            <FormLabel id="setting-form-closure-mode">
              {t('settings.list.formClosureMode')}
            </FormLabel>
            <RadioGroup
              aria-labelledby="setting-form-closure-mode"
              defaultValue={formClosureMode}
              name="radio-buttons-group"
              onChange={() => toggleFormClosureMode()}
            >
              {ALL_FORM_CLOSURE_MODES.map((formClosureMode: FormClosureMode, index: number) => (
                <React.Fragment key={index}>
                  <FormControlLabel
                    value={formClosureMode}
                    control={<Radio />}
                    label={t(`formClosureMode.${formClosureMode}`)}
                  />
                </React.Fragment>
              ))}
            </RadioGroup>
          </FormControl>

          <FormControl>
            <FormLabel id="setting-color-set">{t('settings.list.colorSet')}</FormLabel>
            <RadioGroup
              aria-labelledby="setting-color-set"
              defaultValue={colorSet}
              name="radio-buttons-group"
              onChange={(_event: ChangeEvent, value: string) => {
                setColorSet(value as ColorSet);
              }}
            >
              {ALL_COLOR_SETS.map((colorSet: ColorSet, index: number) => (
                <React.Fragment key={index}>
                  <FormControlLabel
                    value={colorSet}
                    control={<Radio />}
                    label={t(`colorSet.${colorSet}`)}
                  />
                  <ColorList colors={colorsByColorSet(colorSet)} />
                </React.Fragment>
              ))}
            </RadioGroup>
          </FormControl>
        </Stack>
      </Stack>
    </Drawer>
  );
};
