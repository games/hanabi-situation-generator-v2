import { useAppDispatch } from '@/app/store';
import { colorsByColorSet } from '@/features/settings/helpers/color-mapping';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import {
  selectSettings,
  setColorSet,
  setDefaultNbOfPlayers,
  toggleCardDrawDirection,
  toggleFormClosureMode,
  toggleHideDeck,
} from '@/features/settings/slices/settingSlice.ts';
import { setNbRemainingCards } from '@/features/configurator/slices/deckSlice';
import { useSelector } from 'react-redux';

export const useSettingDrawer = () => {
  const dispatch = useAppDispatch();
  const { colorSet, hideDeck, cardDrawDirection, defaultNbOfPlayers, formClosureMode } =
    useSelector(selectSettings);

  return {
    colorSet,
    defaultNbOfPlayers,
    hideDeck,
    cardDrawDirection,
    formClosureMode,
    colorsByColorSet,
    toggleHideDeck: () => {
      if (!hideDeck) {
        dispatch(setNbRemainingCards(undefined));
      }
      dispatch(toggleHideDeck());
    },
    setColorSet: (colorSet: ColorSet) => dispatch(setColorSet(colorSet)),
    setDefaultNbOfPlayers: (defaultNbOfPLayers: number) =>
      dispatch(setDefaultNbOfPlayers(defaultNbOfPLayers)),
    toggleCardDrawDirection: () => dispatch(toggleCardDrawDirection()),
    toggleFormClosureMode: () => dispatch(toggleFormClosureMode()),
  };
};
