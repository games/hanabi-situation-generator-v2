import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { ALL_COLORS } from '@/features/situation/model/color/AllColors.const';
import { EXTENDED_HGROUP_COLORS } from '@/features/situation/model/color/ExtendedHGroupColors.const';
import { HGROUP_COLORS } from '@/features/situation/model/color/HGroupColors.const';
import { STANDARD_COLORS } from '@/features/situation/model/color/StandardColors.const';

export const colorsByColorSet = (colorSet: ColorSet) => {
  return {
    [ColorSet.ALL]: ALL_COLORS,
    [ColorSet.STANDARD]: STANDARD_COLORS,
    [ColorSet.HGROUP]: HGROUP_COLORS,
    [ColorSet.EXTENDED_HGROUP]: EXTENDED_HGROUP_COLORS,
  }[colorSet];
};
