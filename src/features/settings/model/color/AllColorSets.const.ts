import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';

export const ALL_COLOR_SETS = Object.values(ColorSet);
