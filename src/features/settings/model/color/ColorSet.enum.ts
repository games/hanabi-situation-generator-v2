export enum ColorSet {
  STANDARD = 'standard',
  HGROUP = 'h-group',
  EXTENDED_HGROUP = 'extended_h-group',
  ALL = 'all',
}
