import { CardDrawDirection } from '@/features/settings/model/setting/CardDrawDirection.enum';

export const ALL_CARD_DRAW_DIRECTIONS = Object.values(CardDrawDirection);
