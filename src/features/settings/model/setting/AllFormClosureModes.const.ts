import { FormClosureMode } from '@/features/settings/model/setting/FormClosureMode.enum.ts';

export const ALL_FORM_CLOSURE_MODES = Object.values(FormClosureMode);
