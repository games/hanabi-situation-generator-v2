export enum CardDrawDirection {
  FROM_LEFT_TO_RIGHT = 'fromLeftToRight',
  FROM_RIGHT_TO_LEFT = 'fromRightToLeft',
}
