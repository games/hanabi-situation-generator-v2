import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { CardDrawDirection } from '@/features/settings/model/setting/CardDrawDirection.enum';
import { FormClosureMode } from '@/features/settings/model/setting/FormClosureMode.enum.ts';

export interface Settings {
  readonly: boolean;
  defaultNbOfPlayers: number;
  hideDeck: boolean;
  colorSet: ColorSet;
  cardDrawDirection: CardDrawDirection;
  formClosureMode: FormClosureMode;
}
