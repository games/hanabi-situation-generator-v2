export enum FormClosureMode {
  ON_CHANGE = 'onChange',
  ON_CLICK_OUT = 'onClickOut',
}
