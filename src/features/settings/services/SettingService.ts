import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { CardDrawDirection } from '@/features/settings/model/setting/CardDrawDirection.enum';
import { Settings } from '@/features/settings/model/setting/Config.interface';
import { STORAGE_KEY_SETTINGS } from '@/features/settings/model/setting/StorageKeySettings.ts';
import { FormClosureMode } from '@/features/settings/model/setting/FormClosureMode.enum.ts';

export const SettingService = () => {
  return {
    saveToStorage: (settings: Settings) => {
      localStorage.setItem(STORAGE_KEY_SETTINGS, JSON.stringify(settings));
    },
    getFromStorageOrDefault: (): Settings => {
      const dataFromStorage = localStorage.getItem(STORAGE_KEY_SETTINGS);
      if (dataFromStorage) {
        return JSON.parse(dataFromStorage) as Settings;
      }

      return {
        readonly: false,
        defaultNbOfPlayers: 4,
        hideDeck: false,
        colorSet: ColorSet.ALL,
        cardDrawDirection: CardDrawDirection.FROM_LEFT_TO_RIGHT,
        formClosureMode: FormClosureMode.ON_CLICK_OUT,
      };
    },
  };
};
