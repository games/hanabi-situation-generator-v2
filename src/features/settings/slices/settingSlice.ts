import { RootState } from '@/app/store';
import { ColorSet } from '@/features/settings/model/color/ColorSet.enum';
import { CardDrawDirection } from '@/features/settings/model/setting/CardDrawDirection.enum';
import { SettingService } from '@/features/settings/services/SettingService.ts';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { FormClosureMode } from '@/features/settings/model/setting/FormClosureMode.enum.ts';

const { getFromStorageOrDefault } = SettingService();

const settingSlice = createSlice({
  name: 'config',
  initialState: {
    ...getFromStorageOrDefault(),
  },
  reducers: {
    setReadonly: (state, action: PayloadAction<boolean>) => {
      state.readonly = action.payload;
    },
    setDefaultNbOfPlayers: (state, action: PayloadAction<number>) => {
      state.defaultNbOfPlayers = action.payload;
    },
    toggleReadonly: (state) => {
      state.readonly = !state.readonly;
    },
    toggleHideDeck: (state) => {
      state.hideDeck = !state.hideDeck;
    },
    toggleCardDrawDirection: (state) => {
      state.cardDrawDirection =
        state.cardDrawDirection === CardDrawDirection.FROM_LEFT_TO_RIGHT
          ? CardDrawDirection.FROM_RIGHT_TO_LEFT
          : CardDrawDirection.FROM_LEFT_TO_RIGHT;
    },
    toggleFormClosureMode: (state) => {
      state.formClosureMode =
        state.formClosureMode === FormClosureMode.ON_CHANGE
          ? FormClosureMode.ON_CLICK_OUT
          : FormClosureMode.ON_CHANGE;
    },
    setColorSet: (state, action: PayloadAction<ColorSet>) => {
      state.colorSet = action.payload;
    },
  },
});

export const settingReducer = settingSlice.reducer;

export const {
  setColorSet,
  setReadonly,
  setDefaultNbOfPlayers,
  toggleReadonly,
  toggleFormClosureMode,
  toggleHideDeck,
  toggleCardDrawDirection,
} = settingSlice.actions;

export const selectSettings = (state: RootState) => state.settings;
