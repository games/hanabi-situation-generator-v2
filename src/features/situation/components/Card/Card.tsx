import React from 'react';
import { Card as CardMui, Paper, Stack } from '@mui/material';
import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';
import { ButtonWrapper } from '@/shared/components/ButtonWrapper/ButtonWrapper';
import styles from './card.module.css';

type Props = {
  value?: number | Value;
  valueReplacement?: string;
  color?: Color;
  onClick?: () => void;
  title?: string;
  realColor?: Color;
  realValue?: Value;
};

export const Card = ({
  value,
  color,
  valueReplacement,
  onClick,
  title,
  realColor,
  realValue,
}: Props) => {
  const displayRealColor = realColor && color !== realColor;
  const displayRealValue = realValue && value !== realValue;

  return (
    <CardMui variant="outlined" className={styles.container}>
      <ButtonWrapper onClick={onClick} title={title}>
        <Stack
          className={`
            card 
            ${styles.innerContainer} 
            ${color ? `color-${color}` : ''} 
          `}
          alignItems="center"
        >
          <Stack className={styles.knownValue} direction="row">
            {value && <span className={styles.value}>{value}</span>}
            {valueReplacement && !value && <>{valueReplacement}</>}
          </Stack>

          {(displayRealColor || displayRealValue) && (
            <Paper
              className={`
                ${styles.realDataContainer} 
                color-${displayRealColor ? realColor : 'undefined'}
              `}
              sx={{ boxShadow: '0 0 5px 1px #888' }}
            >
              <span className={styles.realValue}>
                {displayRealValue ? <>{realValue}</> : <>&nbsp;</>}
              </span>
            </Paper>
          )}
        </Stack>
      </ButtonWrapper>
    </CardMui>
  );
};
