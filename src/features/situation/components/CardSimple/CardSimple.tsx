import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';
import { ButtonWrapper } from '@/shared/components/ButtonWrapper/ButtonWrapper';
import { Box, Card as CardMui } from '@mui/material';
import React from 'react';
import styles from './cardSimple.module.css';

type Props = {
  value?: number | Value;
  valueReplacement?: string;
  color?: Color;
  onClick?: () => void;
  title?: string;
};

export const CardSimple = ({ value, color, valueReplacement, onClick, title }: Props) => {
  return (
    <CardMui variant="outlined" className={styles.container}>
      <ButtonWrapper onClick={onClick} title={title}>
        <Box
          className={`
            flexBox 
            card 
            ${styles.innerContainer} 
            ${color ? `color-${color}` : ''} 
          `}
        >
          {value && <>{value}</>}
          {valueReplacement && !value && <>{valueReplacement}</>}
        </Box>
      </ButtonWrapper>
    </CardMui>
  );
};
