import { Card } from '@/features/situation/components/Card/Card';
import { CardWrapper } from '@/features/situation/components/CardWrapper/CardWrapper';
import { Clue } from '@/features/situation/components/Clue/Clue';
import { EmptySlot } from '@/features/situation/components/EmptySlot/EmptySlot';
import { Card as CardModel } from '@/features/situation/model/card/Card.interface';
import { Box } from '@mui/material';
import React from 'react';
import styles from './cardWithClue.module.css';
import { MdArrowDownward } from 'react-icons/md';

type Props = {
  card: CardModel;
  isHovered: boolean;
  cardTitle?: string;
  onCardClick?: () => void;
  clue1Title?: string;
  clue2Title?: string;
  onClueClick?: (index: number) => void;
  readonly?: boolean;
};

export const CardWithClues = ({
  card,
  isHovered,
  onCardClick,
  cardTitle,
  onClueClick,
  clue1Title,
  clue2Title,
  readonly = false,
}: Props) => {
  const neutralCallback = () => {};

  return (
    <CardWrapper canHaveClue>
      {((!readonly && isHovered) || card?.clue1) && (
        <Box className={`${styles.clue} ${!readonly || card?.clue2 ? styles.clue1 : ''}`}>
          {card?.clue1 ? (
            <Clue
              clue={card.clue1}
              onClick={onClueClick ? () => onClueClick(0) : undefined}
              title={clue1Title}
            />
          ) : (
            <EmptySlot
              onClick={onClueClick ? () => onClueClick(0) : neutralCallback}
              title={clue1Title}
              emptyIcon={<MdArrowDownward size={'70%'} />}
            />
          )}
        </Box>
      )}

      {((!readonly && isHovered) || card?.clue2) && (
        <Box className={`${styles.clue} ${!readonly || card?.clue1 ? styles.clue2 : ''}`}>
          {card?.clue2 ? (
            <Clue
              clue={card.clue2}
              onClick={onClueClick ? () => onClueClick(1) : undefined}
              title={clue2Title}
            />
          ) : (
            <EmptySlot
              onClick={onClueClick ? () => onClueClick(1) : neutralCallback}
              title={clue2Title}
              emptyIcon={<MdArrowDownward size={'70%'} />}
            />
          )}
        </Box>
      )}

      <Card
        color={card.knownColor}
        value={card.knownValue}
        onClick={onCardClick}
        title={cardTitle}
        realValue={card.realValue}
        realColor={card.realColor}
      />
    </CardWrapper>
  );
};
