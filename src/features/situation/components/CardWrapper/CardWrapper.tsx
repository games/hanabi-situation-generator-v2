import { Box } from '@mui/material';
import { ReactNode } from 'react';
import styles from './cardWrapper.module.css';

type Props = {
  canHaveClue?: boolean;
  small?: boolean;
  children?: ReactNode;
};

export const CardWrapper = ({ canHaveClue = false, small = false, children }: Props) => {
  return (
    <Box
      className={`
        ${styles.container} 
        ${canHaveClue ? styles.canHaveClue : ''} 
        ${small ? styles.small : ''}
      `}
    >
      {children}
    </Box>
  );
};
