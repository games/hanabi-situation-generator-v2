import { Clue as ClueModel } from '@/features/situation/model/clue/Clue.interface';
import { ButtonWrapper } from '@/shared/components/ButtonWrapper/ButtonWrapper';
import { Box } from '@mui/material';
import React from 'react';
import styles from './clue.module.css';

type Props = {
  clue: ClueModel;
  onClick?: () => void;
  title?: string;
};

export const Clue = ({ clue, onClick, title }: Props) => {
  return (
    <Box className={styles.container}>
      <ButtonWrapper onClick={onClick} title={title}>
        <Box
          className={styles.arrow}
          sx={
            clue.number
              ? {
                  '&::before': {
                    content: `"${clue.number}"`,
                    top: !!onClick ? '-4px' : undefined,
                    left: !!onClick ? '-5px' : undefined,
                  },
                }
              : undefined
          }
        ></Box>

        <Box
          className={`
          ${styles.clueContent} 
          ${clue.color ? `${styles.clueColor} color-${clue.color}` : ''} 
          ${clue.value ? `${styles.clueValue} value-${clue.value}` : ''} 
        `}
        >
          {clue.value && <>{clue.value}</>}
        </Box>
      </ButtonWrapper>
    </Box>
  );
};
