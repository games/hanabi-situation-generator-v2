import { Color } from '@/features/situation/model/color/Color.enum';
import { Box, Stack } from '@mui/material';
import { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import styles from './colorList.module.css';

type Props = {
  colors: Color[];
  compact?: boolean;
};

export const ColorList = ({ colors, compact = false }: Props) => {
  const { t } = useTranslation();

  return (
    <Stack
      className={styles.container}
      direction={compact ? 'row' : 'column'}
      spacing={1}
      alignItems={compact ? 'center' : 'column'}
    >
      <Stack direction="row" spacing={0.5} className={styles.list}>
        {colors.map((color: Color, index: number) => (
          <Fragment key={index}>
            <Box
              className={`
              ${styles.color} 
              ${color ? styles[`color-${color}`] : ''} 
              ${color ? `color-${color}` : ''}
            `}
              title={t(`color.list.${color}`)}
            ></Box>
          </Fragment>
        ))}
      </Stack>
    </Stack>
  );
};
