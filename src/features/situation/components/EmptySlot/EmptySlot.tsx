import { Button, Card } from '@mui/material';
import React, { useRef } from 'react';
import { BiSolidHide } from 'react-icons/bi';
import { MdAdd } from 'react-icons/md';
import { useHover } from 'usehooks-ts';
import styles from './emptySlot.module.css';

type Props = {
  onClick: () => void;
  title?: string;
  emptyIcon?: JSX.Element;
  addIcon?: JSX.Element;
};

export const EmptySlot = ({ onClick, title, emptyIcon, addIcon }: Props) => {
  const container = useRef(null);
  const isHover = useHover(container);

  return (
    <Card
      ref={container}
      variant="outlined"
      className={`
        ${styles.container}
        flexBox
      `}
    >
      <Button
        className={`flexBox ${styles.button}`}
        sx={{ color: isHover ? 'primary' : 'grey.200' }}
        onClick={() => onClick && onClick()}
        title={title}
      >
        {isHover ? (
          <>{addIcon || <MdAdd size={'70%'} />}</>
        ) : (
          <>{emptyIcon || <BiSolidHide size={'70%'} />}</>
        )}
      </Button>
    </Card>
  );
};
