import { Color } from '@/features/situation/model/color/Color.enum';
import { Box, Button, Stack } from '@mui/material';
import { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { MdBlock } from 'react-icons/md';
import styles from './colorField.module.css';

type Props = {
  label?: string;
  colors: Color[];
  onChange: (color: Color | undefined) => void;
  selected: Color | undefined;
  allowUndefinedColor?: boolean;
  compact?: boolean;
};

export const ColorField = ({
  label,
  colors,
  onChange,
  selected,
  allowUndefinedColor = true,
  compact = false,
}: Props) => {
  const { t } = useTranslation();
  const renderItem = (color: Color | undefined) => (
    <Button
      className={`
        ${styles.color} 
        ${color ? styles[`color-${color}`] : ''} 
        ${selected === color ? styles.selected : ''} 
        ${color ? `color-${color}` : ''}
      `}
      title={t(`color.list.${color}`)}
      onClick={() => onChange(color)}
      variant="outlined"
    >
      {!color && <MdBlock size={'1rem'} />}
    </Button>
  );

  return (
    <Stack
      className={styles.container}
      direction={compact ? 'row' : 'column'}
      spacing={0.5}
      alignItems={compact ? 'center' : 'column'}
      useFlexGap
      flexWrap="wrap"
    >
      {label && <Box className={styles.label}>{label}</Box>}
      <Stack direction="row" spacing={0.5} className={styles.list}>
        {allowUndefinedColor && renderItem(undefined)}
        {colors.map((color: Color, index: number) => (
          <Fragment key={index}>{renderItem(color)}</Fragment>
        ))}
      </Stack>
    </Stack>
  );
};
