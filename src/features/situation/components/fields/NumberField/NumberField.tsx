import { IconButton, InputAdornment, TextField } from '@mui/material';
import React, { BaseSyntheticEvent } from 'react';
import { useTranslation } from 'react-i18next';
import { IoCloseOutline } from 'react-icons/io5';
import styles from './numberField.module.css';

type Props = {
  label?: string;
  value: number | undefined;
  onChange: (value: number | undefined) => void;
  onBlur?: () => void;
  min?: number;
  max?: number;
};

export const NumberField = ({ label, value, onChange, onBlur, min, max }: Props) => {
  const { t } = useTranslation();

  return (
    <TextField
      label={label}
      className={styles.container}
      value={value ?? ''}
      size="small"
      variant="standard"
      onChange={(event: BaseSyntheticEvent) => {
        onChange(event.target.value !== '' ? +event.target.value : undefined);
      }}
      onBlur={onBlur}
      inputProps={{
        min: min || 0,
        max: max || 99,
        type: 'number',
      }}
      InputProps={{
        endAdornment: value ? (
          <InputAdornment position="end">
            <IconButton onClick={() => onChange(undefined)} title={t('_shared.action.empty')}>
              <IoCloseOutline />
            </IconButton>
          </InputAdornment>
        ) : undefined,
      }}
    />
  );
};
