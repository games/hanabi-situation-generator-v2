import { Value } from '@/features/situation/model/value/Value.enum';
import { Box, Button, Stack } from '@mui/material';
import { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { MdBlock } from 'react-icons/md';
import styles from './valueField.module.css';

type Props = {
  label?: string;
  values: Value[];
  onChange: (value: Value | undefined) => void;
  selected: Value | undefined;
  compact?: boolean;
};

export const ValueField = ({ label, values, onChange, selected, compact = false }: Props) => {
  const { t } = useTranslation();
  const renderItem = (value: number | undefined) => (
    <Button
      className={`
        ${styles.value} 
        ${value ? styles[`value-${value}`] : ''} 
        ${selected === value ? styles.selected : ''} 
        value-${value}
      `}
      onClick={() => onChange(value)}
      variant="outlined"
      title={value ? undefined : t('value.undefined')}
    >
      {value ? <>{value}</> : <MdBlock size={'1rem'} />}
    </Button>
  );

  return (
    <Stack
      className={styles.container}
      direction={compact ? 'row' : 'column'}
      spacing={1}
      alignItems={compact ? 'center' : 'column'}
    >
      {label && <Box className={styles.label}>{label}</Box>}
      <Stack direction="row" spacing={0.5} className={styles.list}>
        {renderItem(undefined)}
        {values.map((value: Value, index: number) => (
          <Fragment key={index}>{renderItem(value)}</Fragment>
        ))}
      </Stack>
    </Stack>
  );
};
