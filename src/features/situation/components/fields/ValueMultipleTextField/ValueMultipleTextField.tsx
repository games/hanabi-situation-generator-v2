import { ALL_VALUES } from '@/features/situation/model/value/AllValues.const';
import { Value } from '@/features/situation/model/value/Value.enum';
import { TextField } from '@mui/material';
import React, { BaseSyntheticEvent, useState } from 'react';

type Props = {
  label?: string;
  helperText?: string;
  onBlur: (values: Value[]) => void;
  values: Value[];
};

export const ValueMultipleTextField = ({ label, helperText, values, onBlur }: Props) => {
  const convertValuesFromFieldValue = (string: string) => {
    const cleanedStringValue: unknown[] = string
      .replace(/ +/g, ',')
      .replace(/,+/g, ',')
      .split(',')
      .filter((stringValue: string) => ALL_VALUES.includes(+stringValue))
      .sort((value1, value2) => value1.localeCompare(value2));

    return cleanedStringValue as Value[];
  };
  const convertValuesToFieldValue = (values: Value[]) => values.join(' ');

  const [fieldValue, setFieldValue] = useState(convertValuesToFieldValue(values));

  return (
    <TextField
      label={label}
      value={fieldValue}
      size="small"
      variant="standard"
      onChange={(event: BaseSyntheticEvent) => {
        setFieldValue(event.target.value);
      }}
      onBlur={() => {
        onBlur && onBlur(convertValuesFromFieldValue(fieldValue));
      }}
      helperText={helperText}
    />
  );
};
