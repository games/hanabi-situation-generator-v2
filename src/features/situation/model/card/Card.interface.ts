import { Clue } from '@/features/situation/model/clue/Clue.interface';
import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';

export interface Card {
  id: string;
  knownColor?: Color;
  knownValue?: Value;
  realColor?: Color;
  realValue?: Value;
  clue1?: Clue;
  clue2?: Clue;
}
