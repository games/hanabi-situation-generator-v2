import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';

export interface Clue {
  color?: Color;
  value?: Value;
  number?: number;
}
