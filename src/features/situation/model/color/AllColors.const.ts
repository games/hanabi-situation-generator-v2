import { Color } from '@/features/situation/model/color/Color.enum';

export const ALL_COLORS = Object.values(Color);
