import { Color } from '@/features/situation/model/color/Color.enum';
import { HGROUP_COLORS } from '@/features/situation/model/color/HGroupColors.const';

export const EXTENDED_HGROUP_COLORS = [
  ...HGROUP_COLORS,
  Color.RAINBOW,
  Color.PINK,
  Color.BROWN,
  Color.NULL,
  Color.LIGHT_PINK,
  Color.GRAY,
  Color.WHITE,
];
