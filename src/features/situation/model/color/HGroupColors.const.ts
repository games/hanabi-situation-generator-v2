import { Color } from '@/features/situation/model/color/Color.enum';

export const HGROUP_COLORS = [Color.BLUE, Color.GREEN, Color.PURPLE, Color.RED, Color.TEAL];
