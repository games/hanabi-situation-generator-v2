import { Color } from '@/features/situation/model/color/Color.enum';

export const STANDARD_COLORS = [
  Color.RED,
  Color.YELLOW,
  Color.GREEN,
  Color.BLUE,
  Color.WHITE,
  Color.MULTICOLOR,
  Color.BLACK,
];
