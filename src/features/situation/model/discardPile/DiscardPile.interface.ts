import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';
import { PartialRecord } from '@/shared/model/partialRecord.type';

export type DiscardPile = PartialRecord<Color, Value[]>;
