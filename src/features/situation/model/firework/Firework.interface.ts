import { Color } from '@/features/situation/model/color/Color.enum';
import { Value } from '@/features/situation/model/value/Value.enum';

export interface Firework {
  color?: Color;
  value?: Value;
}
