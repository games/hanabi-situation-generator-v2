import { NbOfPlayers } from '@/features/situation/model/player/NbOfPlayers.enum.ts';

export const ALL_NB_OF_PLAYERS = Object.values(NbOfPlayers);
