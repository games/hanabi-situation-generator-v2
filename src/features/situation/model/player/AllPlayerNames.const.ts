import { PlayerName } from '@/features/situation/model/player/PlayerName.enum';

export const ALL_PLAYER_NAMES = Object.values(PlayerName);
