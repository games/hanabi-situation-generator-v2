export enum NbOfPlayers {
  TWO = '2',
  THREE = '3',
  FOUR = '4',
  FIVE = '5',
}
