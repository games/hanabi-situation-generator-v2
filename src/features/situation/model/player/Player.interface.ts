import { Card } from '@/features/situation/model/card/Card.interface';
import { PlayerName } from '@/features/situation/model/player/PlayerName.enum';

export interface Player {
  id: string;
  name: string | PlayerName;
  hand: Card[];
}
