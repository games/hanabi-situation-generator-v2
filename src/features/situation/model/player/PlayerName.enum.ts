export enum PlayerName {
  ALICE = 'Alice',
  BOB = 'Bob',
  CAROLE = 'Carole',
  DAVID = 'David',
  EVE = 'Eve',
}
