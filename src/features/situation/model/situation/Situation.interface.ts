import { Deck } from '@/features/situation/model/deck/Deck.interface';
import { DiscardPile } from '@/features/situation/model/discardPile/DiscardPile.interface';
import { Firework } from '@/features/situation/model/firework/Firework.interface';
import { Player } from '@/features/situation/model/player/Player.interface';

export interface Situation {
  deck?: Deck;
  fireworks?: Firework[];
  discardPile?: DiscardPile;
  players?: Player[];
}
