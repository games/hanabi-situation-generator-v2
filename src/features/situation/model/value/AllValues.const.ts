import { Value } from '@/features/situation/model/value/Value.enum';

export const ALL_VALUES = Object.values(Value).filter((value) => !isNaN(Number(value))) as Value[];
