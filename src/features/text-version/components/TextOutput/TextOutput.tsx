import { Situation } from '@/features/situation/model/situation/Situation.interface';
import { TextOutputService } from '@/features/text-version/model/TextOutputService';
import { NotificationSnack } from '@/shared/components/NotificationSnack/NotificationSnack';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { LuClipboardPaste } from 'react-icons/lu';
import styles from './textOutput.module.css';

type Props = {
  opened: boolean;
  onClose: () => void;
  situation: Situation;
};

export const TextOutput = ({ opened, onClose, situation }: Props) => {
  const { t } = useTranslation();

  const { convertToText } = TextOutputService();
  const textVersion = convertToText(situation);

  const [copySuccess, setCopySuccess] = useState(false);
  const onCopy = () => {
    setCopySuccess(true);
    navigator.clipboard.writeText(textVersion);
  };

  return (
    <>
      <Dialog
        open={opened}
        onClose={onClose}
        aria-labelledby="text-version-modal-title"
        fullWidth={true}
      >
        <DialogTitle id="text-version-modal-title">{t('exportText.label')}</DialogTitle>
        <DialogContent dividers>
          <Box className={styles.textVersion}>
            <code>{convertToText(situation)}</code>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => onClose()} variant="outlined">
            {t('exportText.action.close')}
          </Button>
          <Button
            variant="contained"
            color="primary"
            startIcon={<LuClipboardPaste />}
            onClick={() => {
              onCopy();
              onClose();
            }}
            autoFocus
          >
            {t('exportText.action.copy')}
          </Button>
        </DialogActions>
      </Dialog>

      <NotificationSnack
        opened={copySuccess}
        onDismiss={() => setCopySuccess(false)}
        type="success"
      >
        {t('exportText.textVersionCopied')}
      </NotificationSnack>
    </>
  );
};
