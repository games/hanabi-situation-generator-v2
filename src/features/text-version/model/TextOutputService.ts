import { Card } from '@/features/situation/model/card/Card.interface';
import { Color } from '@/features/situation/model/color/Color.enum';
import { Player } from '@/features/situation/model/player/Player.interface';
import { Situation } from '@/features/situation/model/situation/Situation.interface';

export const TextOutputService = () => {
  return {
    convertToText,
  };
};

const convertColorToText = (color: Color | undefined) => {
  if (!color) {
    return 'X';
  }

  const colorShortNames = {
    [Color.BLUE]: 'B',
    [Color.GREEN]: 'G',
    [Color.RED]: 'R',
    [Color.WHITE]: 'W',
    [Color.YELLOW]: 'Y',
    [Color.MULTICOLOR]: 'M',
    [Color.BLACK]: 'Bk',
    [Color.TEAL]: 'T',
    [Color.PURPLE]: 'P',
    [Color.RAINBOW]: 'Rw',
    [Color.PINK]: 'Pk',
    [Color.BROWN]: 'Bn',
    [Color.NULL]: 'N',
    [Color.LIGHT_PINK]: 'LP',
    [Color.GRAY]: 'Gy',
  };

  return colorShortNames[color] as string;
};

const convertCardToText = (card: Card) => {
  let textCard = convertColorToText(card.knownColor ?? card.realColor);
  textCard += card.knownValue ?? card.realValue ?? 'X';

  // On force la carte à tenir sur 3 caractères pour conserver l'alignement,
  // malgré que certaines couleurs soient sur 2 caractères
  return textCard.padEnd(3);
};
const convertPLayerToText = (player: Player) => {
  return `${player.name[0]}: ${player.hand.map(convertCardToText).join(' ')}`;
};

const convertToText = (situation: Situation) => {
  return (situation.players ?? []).map(convertPLayerToText).join('\n');
};
