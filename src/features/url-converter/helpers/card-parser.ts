import {
  buildRegExpColorList,
  buildRegExpValueList,
} from '@/features/url-converter/helpers/regexp';
import { CONFIG } from '@/features/url-converter/model/Config.const';
import { LegacyCard } from '@/features/url-converter/model/LegacyCard.interface';
import { LegacyColor } from '@/features/url-converter/model/LegacyColor.enum';

export const getParser = (availableColors?: string[]) => {
  return (cardString: string): LegacyCard | null => {
    const cleanCardString = cardString.trim();
    if (cleanCardString.length === 0) {
      return null;
    }

    const cardRegexp = getCardRegexp(availableColors);
    const groups = cleanCardString.match(cardRegexp);
    if (groups === null) {
      return null;
    }
    const clue = groups[1] || '';
    let knownColor = groups[5] || groups[13] || groups[15] || null;
    knownColor = knownColor !== 'X' ? knownColor : null;
    let realColor = groups[8] || groups[14] || null;
    realColor = realColor !== 'X' ? realColor : null;
    let knownValue: string | number | null = groups[6] || groups[18] || groups[20] || null;
    knownValue = knownValue !== null && knownValue !== 'X' ? +knownValue : null;
    let realValue: string | number | null = groups[10] || groups[19] || null;
    realValue = realValue !== null && realValue !== 'X' ? +realValue : null;

    return {
      color: {
        known: knownColor as LegacyColor | null,
        real: (realColor || knownColor) as LegacyColor | null,
      },
      value: {
        known: knownValue,
        real: realValue || knownValue,
      },
      clued: !!clue,
      clued1: clue.includes('!'),
      clued2: clue.includes('?'),
      title: cleanCardString,
    };
  };
};
// ((<C>)?[(](<C>)[)])|(<C>)
const REGEXP_PART_CARD_COLOR =
  '((<REGEXP_PART_COLOR_LIST>)?[(](<REGEXP_PART_COLOR_LIST>)[)])|(<REGEXP_PART_COLOR_LIST>)';
// ((<V>)?[(](<V>)[)])|(<V>)
const REGEXP_PART_CARD_VALUE =
  '((<REGEXP_PART_VALUE_LIST>)?[(](<REGEXP_PART_VALUE_LIST>)[)])|(<REGEXP_PART_VALUE_LIST>)';
// ((<C>)(<V>)?)?([(](<C>))((<V>)[)])
const REGEXP_PART_CARD_COMBINED =
  '((<REGEXP_PART_COLOR_LIST>)(<REGEXP_PART_VALUE_LIST>)?)?([(](<REGEXP_PART_COLOR_LIST>))((<REGEXP_PART_VALUE_LIST>)[)])';
// ([!]?)(( ((<C>)(<V>)?)?([(](<C>))((<V>)[)]) )|( ((<C>)?[(](<C>)[)])|(<C>) )( ((<V>)?[(](<V>)[)])|(<V>) ))
const CARD_REGEXP_TEMPLATE = `([!?]*)((${REGEXP_PART_CARD_COMBINED})|(${REGEXP_PART_CARD_COLOR})(${REGEXP_PART_CARD_VALUE}))`;
const CARD_REGEXP = CARD_REGEXP_TEMPLATE.replace(
  /<REGEXP_PART_COLOR_LIST>/g,
  buildRegExpColorList(CONFIG.AVAILABLE_COLORS),
).replace(/<REGEXP_PART_VALUE_LIST>/g, buildRegExpValueList());
const defaultCardRegexp = new RegExp(CARD_REGEXP);
const getCardRegexp = (availableColors?: string[]) => {
  return availableColors
    ? CARD_REGEXP_TEMPLATE.replace(
        /<REGEXP_PART_COLOR_LIST>/g,
        buildRegExpColorList(availableColors),
      ).replace(/<REGEXP_PART_VALUE_LIST>/g, buildRegExpValueList())
    : defaultCardRegexp;
};
