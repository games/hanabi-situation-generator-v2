import {
  buildRegExpColorList,
  buildRegExpValueList,
} from '@/features/url-converter/helpers/regexp';
import { CONFIG } from '@/features/url-converter/model/Config.const';
import { LegacyClue } from '@/features/url-converter/model/LegacyClue.interface';

export const getParser = (availableColors?: string[]) => {
  return (clueString: string): LegacyClue | null => {
    const cleanClueString = clueString.replace(' ', '');
    if (cleanClueString.length === 0) {
      return null;
    }

    const clueRegexp = getClueRegexp(availableColors);
    const groups = cleanClueString.match(clueRegexp);
    if (groups === null) {
      return null;
    }
    const color = groups[2];
    const value = groups[3];

    return {
      type: color ? 'color' : 'value',
      content: color || +value,
    };
  };
};
const CLUE_REGEXP_TEMPLATE = '^((<REGEXP_PART_COLOR_LIST>)|(<REGEXP_PART_VALUE_LIST>))$';
const CLUE_REGEXP = CLUE_REGEXP_TEMPLATE.replace(
  /<REGEXP_PART_COLOR_LIST>/g,
  buildRegExpColorList(CONFIG.AVAILABLE_COLORS, false),
).replace(/<REGEXP_PART_VALUE_LIST>/g, buildRegExpValueList(false));
const defaultClueRegexp = new RegExp(CLUE_REGEXP);
const getClueRegexp = (availableColors?: string[]) => {
  return availableColors
    ? CLUE_REGEXP_TEMPLATE.replace(
        /<REGEXP_PART_COLOR_LIST>/g,
        buildRegExpColorList(availableColors, false),
      ).replace(/<REGEXP_PART_VALUE_LIST>/g, buildRegExpValueList(false))
    : defaultClueRegexp;
};
