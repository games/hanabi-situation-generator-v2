import { ALL_PLAYER_NAMES } from '@/features/situation/model/player/AllPlayerNames.const';
import { QueryParamSituationData } from '@/features/url-converter/model/QueryParamSituationData.interface';
import { getUrlQueryParams } from '@/shared/helpers/url';

export const getSituationDataFromUrl = (url: string): QueryParamSituationData => {
  const queryParams = getUrlQueryParams(url);
  return {
    discardedCards: queryParams.has('discardedCards')
      ? parseStringList(queryParams.get('discardedCards') as string)
      : [],
    highestCardsPlayed: queryParams.has('highestCardsPlayed')
      ? parseStringList(queryParams.get('highestCardsPlayed') as string)
      : [],
    playersHand: queryParams.has('playersHand')
      ? parsePlayerHandStrings(queryParams.get('playersHand') as string)
      : [],
    playersName: queryParams.has('playersName')
      ? parseStringList(queryParams.get('playersName') as string)
      : ALL_PLAYER_NAMES,
    nbRemainingCardsInDeck: queryParams.has('nbRemainingCardsInDeck')
      ? Number(queryParams.get('nbRemainingCardsInDeck') as string)
      : undefined,
  };
};

const parseStringList = (stringList: string) => {
  if (stringList.trim().length === 0) {
    return [];
  }

  return stringList.split(',').map((value) => value.trim());
};

const parseMultipleStringLists = (multipleStringLists: string) => {
  if (multipleStringLists.trim().length === 0) {
    return [];
  }

  return multipleStringLists.split('|');
};

const parsePlayerHandStrings = (stringList: string) => {
  return parseMultipleStringLists(stringList).map((playerHand) => {
    const parts = playerHand.replaceAll(' ', '').split('<-');

    const cards = parts[0].split(',').map((value) => value.trim());

    if (parts.length < 2) {
      return cards;
    }

    cards.push('<-' + parts[1].trim());

    return cards;
  });
};
