/**
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
 */
export const escapeRegExp = (string: string) => {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
};

export const buildRegExpColorList = (availableColors: string[] = [], acceptRandom = true) => {
  return [...availableColors, ...(acceptRandom ? ['X'] : [])].map(escapeRegExp).join('|');
};

export const buildRegExpValueList = (acceptRandom = true) => {
  const valueList = [...new Array(5)].map((_value, index) => index + 1);
  return [...valueList, ...(acceptRandom ? ['X'] : [])].join('|');
};
