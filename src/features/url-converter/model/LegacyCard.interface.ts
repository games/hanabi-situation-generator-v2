import { LegacyColor } from '@/features/url-converter/model/LegacyColor.enum';

export interface LegacyCard {
  color: {
    known: LegacyColor | null;
    real: LegacyColor | null;
  };
  value: {
    known: number | null;
    real: number | null;
  };
  clued: boolean;
  clued1: boolean;
  clued2: boolean;
  title: string;
}
