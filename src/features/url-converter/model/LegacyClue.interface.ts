export interface LegacyClue {
  type: 'color' | 'value';
  content: string | number;
}
