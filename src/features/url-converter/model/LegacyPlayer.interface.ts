import { LegacyCard } from '@/features/url-converter/model/LegacyCard.interface';
import { LegacyClue } from '@/features/url-converter/model/LegacyClue.interface';

export interface LegacyPlayer {
  name: string;
  hand: LegacyCard[];
  clues: LegacyClue[];
}
