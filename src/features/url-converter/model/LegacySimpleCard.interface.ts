export interface LegacySimpleCard {
  color: string | null;
  value: number | null;
}
