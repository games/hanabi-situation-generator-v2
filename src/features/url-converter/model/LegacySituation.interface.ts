import { LegacyColor } from '@/features/url-converter/model/LegacyColor.enum';
import { LegacyPlayer } from '@/features/url-converter/model/LegacyPlayer.interface';
import { LegacySimpleCard } from '@/features/url-converter/model/LegacySimpleCard.interface';

export interface LegacySituation {
  players: LegacyPlayer[];
  highestCardsPlayed: LegacySimpleCard[];
  discardedCards: Map<LegacyColor, number[]>;
  nbRemainingCardsInDeck: number | undefined;
}
