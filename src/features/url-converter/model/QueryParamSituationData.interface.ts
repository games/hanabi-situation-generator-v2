export interface QueryParamSituationData {
  playersHand: string[][];
  highestCardsPlayed: string[];
  discardedCards: string[];
  playersName: string[];
  nbRemainingCardsInDeck: number | undefined;
}
