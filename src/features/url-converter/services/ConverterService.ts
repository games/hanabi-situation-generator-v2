import { SituationService } from '@/features/configurator/services/SituationService';
import { Card } from '@/features/situation/model/card/Card.interface';
import { Clue } from '@/features/situation/model/clue/Clue.interface';
import { Color } from '@/features/situation/model/color/Color.enum';
import { DiscardPile } from '@/features/situation/model/discardPile/DiscardPile.interface';
import { Firework } from '@/features/situation/model/firework/Firework.interface';
import { Player } from '@/features/situation/model/player/Player.interface';
import { Situation } from '@/features/situation/model/situation/Situation.interface';
import { Value } from '@/features/situation/model/value/Value.enum';
import { getSituationDataFromUrl } from '@/features/url-converter/helpers/legacy-url-parser';
import { LegacyCard } from '@/features/url-converter/model/LegacyCard.interface';
import { LegacyClue } from '@/features/url-converter/model/LegacyClue.interface';
import { LegacyColor } from '@/features/url-converter/model/LegacyColor.enum';
import { LegacyPlayer } from '@/features/url-converter/model/LegacyPlayer.interface';
import { LegacySimpleCard } from '@/features/url-converter/model/LegacySimpleCard.interface';
import { LegacySituation } from '@/features/url-converter/model/LegacySituation.interface';
import { LegacySituationBuilder } from '@/features/url-converter/services/LegacySituationBuilder';
import { uuid } from '@/shared/helpers/id.ts';

export const ConverterService = () => {
  return {
    convertSituation,
    convertUrl,
  };
};

const { getShareUrl } = SituationService();

const convertSituation = (legacySituation: LegacySituation): Situation => {
  return {
    ...(legacySituation.nbRemainingCardsInDeck
      ? { deck: { nbRemainingCards: legacySituation.nbRemainingCardsInDeck } }
      : {}),
    players: legacySituation.players.map(convertPlayer),
    fireworks: legacySituation.highestCardsPlayed.map(convertFirework),
    discardPile:
      legacySituation.discardedCards.size > 0
        ? convertDiscardPile(legacySituation.discardedCards)
        : undefined,
  };
};

const convertUrl = async (legacyUrl: string, shorten = false): Promise<string> => {
  const legacySituation = getLegacySituationFromUrl(legacyUrl);
  const situation = convertSituation(legacySituation);

  return await getShareUrl(situation, shorten);
};

const getLegacySituationFromUrl = (legacyUrl: string) => {
  const { buildLegacySituation } = LegacySituationBuilder();
  return buildLegacySituation(getSituationDataFromUrl(legacyUrl));
};

const convertColor = (colorShortName: LegacyColor | undefined): Color | undefined => {
  if (!colorShortName) {
    return undefined;
  }

  const colorMap = {
    [LegacyColor.Bk]: Color.BLACK,
    [LegacyColor.BK]: Color.BLACK,
    [LegacyColor.K]: Color.BLACK,
    [LegacyColor.B]: Color.BLUE,
    [LegacyColor.R]: Color.RED,
    [LegacyColor.G]: Color.GREEN,
    [LegacyColor.Y]: Color.YELLOW,
    [LegacyColor.W]: Color.WHITE,
    [LegacyColor.M]: Color.MULTICOLOR,
    [LegacyColor.P]: Color.PURPLE,
    [LegacyColor.T]: Color.TEAL,
    [LegacyColor.Pk]: Color.PINK,
    [LegacyColor.PK]: Color.PINK,
    [LegacyColor.Bn]: Color.BROWN,
    [LegacyColor.BN]: Color.BROWN,
  };

  return colorMap[colorShortName] as Color;
};

const convertClue = (legacyClue: LegacyClue, number: number | undefined = undefined): Clue => {
  return {
    [legacyClue.type]:
      typeof legacyClue.content === 'string'
        ? convertColor(legacyClue.content as LegacyColor)
        : legacyClue.content,
    ...(number ? { number } : {}),
  };
};

const convertPlayer = (legacyPlayer: LegacyPlayer): Player => {
  return {
    id: uuid(),
    name: legacyPlayer.name,
    hand: legacyPlayer.hand.map((legacyCard: LegacyCard) => convertCard(legacyCard, legacyPlayer.clues)),
  };
};

const convertCard = (legacyCard: LegacyCard, clues: LegacyClue[] | undefined): Card => {
  return {
    id: uuid(),
    knownColor: convertColor(legacyCard.color.known || undefined),
    knownValue: legacyCard.value.known || undefined,
    realColor:
      legacyCard.color.real !== legacyCard.color.known
        ? convertColor(legacyCard.color.real || undefined)
        : undefined,
    realValue:
      legacyCard.value.real !== legacyCard.value.known
        ? legacyCard.value.real || undefined
        : undefined,
    ...(clues && clues.length < 2 && legacyCard.clued ? { clue1: convertClue(clues[0]) } : {}),
    ...(clues && clues.length > 1 && legacyCard.clued1
      ? {
        clue1: convertClue(clues[0], 1),
      }
      : {}),
    ...(clues && clues.length > 1 && legacyCard.clued2
      ? {
        clue2: convertClue(clues[1], 2),
      }
      : {}),
  };
};

const convertFirework = (legacyCard: LegacySimpleCard): Firework => {
  return {
    color: convertColor(legacyCard.color as LegacyColor | undefined),
    value: legacyCard.value || undefined,
  };
};

const convertDiscardPile = (legacyDiscardPile: Map<LegacyColor, number[]>): DiscardPile => {
  const list: DiscardPile = {};

  for (const [color, values] of legacyDiscardPile.entries()) {
    list[convertColor(color) as Color] = values as Value[];
  }

  return list;
};
