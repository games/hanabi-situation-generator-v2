import { getParser as getCardParser } from '@/features/url-converter/helpers/card-parser';
import { getParser as getClueParser } from '@/features/url-converter/helpers/clue-parser';
import { CONFIG } from '@/features/url-converter/model/Config.const';
import { LegacyCard } from '@/features/url-converter/model/LegacyCard.interface';
import { LegacyClue } from '@/features/url-converter/model/LegacyClue.interface';
import { LegacyColor } from '@/features/url-converter/model/LegacyColor.enum';
import { LegacySimpleCard } from '@/features/url-converter/model/LegacySimpleCard.interface';
import { LegacySituation } from '@/features/url-converter/model/LegacySituation.interface';
import { QueryParamSituationData } from '@/features/url-converter/model/QueryParamSituationData.interface';

export const LegacySituationBuilder = () => {
  return {
    buildLegacySituation,
  };
};

const buildLegacySituation = ({
  playersHand,
  highestCardsPlayed,
  discardedCards,
  playersName,
  nbRemainingCardsInDeck,
}: QueryParamSituationData): LegacySituation => ({
  players: getPlayers(playersHand, playersName),
  highestCardsPlayed: getPlayed(highestCardsPlayed),
  discardedCards: getDiscarded(discardedCards),
  nbRemainingCardsInDeck,
});

const getPlayed = (cards: string[]): LegacySimpleCard[] => {
  return cards
    .map(getCardParser())
    .filter((card) => card !== null)
    .map((card) => ({
      color: card?.color ? card?.color.real : null,
      value: card?.value ? card?.value.real : null,
    }))
    .filter(({ color }) => color !== null);
};

const getDiscarded = (cards: string[]): Map<LegacyColor, number[]> => {
  return cards
    .map(getCardParser())
    .filter((card) => card !== null)
    .map((card) => ({
      color: card?.color ? card?.color.real : null,
      value: card?.value ? card?.value.real : null,
    }))
    .sort(cardSorter)
    .filter(({ color, value }) => color && value)
    .reduce((discarded, { color, value }) => {
      return discarded.set(color, [...(discarded.get(color) || []), value]);
    }, new Map());
};

const getPlayers = (playersHand: string[][], playersName: string[]) => {
  return playersHand.map((playerHand, index) => {
    const playerName = playersName.length > index ? playersName[index] : CONFIG.PLAYER_NAMES[index];

    const lastItemOfHand = (playerHand[playerHand.length - 1] || '').trim();
    const isHandClued = lastItemOfHand.startsWith('<-');

    return {
      name: playerName,
      hand: getCards(!isHandClued ? playerHand : playerHand.slice(0, playerHand.length - 2)),
      clues: isHandClued ? getClues(lastItemOfHand.substring(2)) : [],
    };
  });
};

const cardSorter = (
  card1: { color: string | null; value: number | null },
  card2: { color: string | null; value: number | null },
) => {
  if (!card1.color) {
    return -1;
  }

  const colorComparison = card1.color.localeCompare(card2.color || 'ZZZ');
  if (colorComparison !== 0) {
    return colorComparison;
  }

  if (!card1.value) {
    return -1;
  }

  return card1.value < (card2.value || 999) ? -1 : 1;
};

const getCards = (playerHand: string[]): LegacyCard[] => {
  return playerHand
    .map(getCardParser())
    .filter((card): card is Exclude<typeof card, null> => card !== null);
};

const getClues = (clueString: string): LegacyClue[] => {
  return clueString
    .trim()
    .split(',')
    .map((clue) => clue.trim())
    .map(getClueParser())
    .filter((clue): clue is Exclude<typeof clue, null> => clue !== null)
    .slice(0, 2);
};
