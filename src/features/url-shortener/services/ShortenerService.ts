import { buildQueryString } from '@/shared/helpers/url.ts';

export const ShortenerService = () => {
  return {
    shortenUrl,
  };
};

const SHORTENER_API_BASE_URL = 'https://spoo.me';

const shortenUrl = async (url: string, alias: string | undefined = undefined): Promise<string> => {
  const dataToSend: { [key: string]: string } = {
    url,
    ...(alias ? { alias } : {}),
  };

  try {
    const response = await fetch(SHORTENER_API_BASE_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'Accept': 'application/json',
      },
      body: buildQueryString(dataToSend),
    });
    const responseData = await response.json();

    if (!responseData.short_url) {
      throw new Error(JSON.stringify(responseData));
    }

    return responseData.short_url;
  } catch (error) {
    console.error(error);
    return url;
  }
};
