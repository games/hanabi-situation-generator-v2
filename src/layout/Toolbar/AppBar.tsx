import { SettingDrawer } from '@/features/settings/components/SettingDrawer/SettingDrawer.tsx';
import { TextOutput } from '@/features/text-version/components/TextOutput/TextOutput';
import { useAppBar } from '@/layout/Toolbar/useAppBar';
import { NotificationSnack } from '@/shared/components/NotificationSnack/NotificationSnack';
import {
  AppBar as AppBarBase,
  Button,
  Fade,
  Grid,
  IconButton,
  Menu,
  MenuItem,
  Stack,
  Toolbar as ToolbarBase,
  Typography,
} from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { BsGearFill } from 'react-icons/bs';
import { LuClipboardPaste } from 'react-icons/lu';
import { MdDownload, MdModeEdit, MdMoreVert, MdOutlineRemoveRedEye } from 'react-icons/md';
import { RxText } from 'react-icons/rx';
import { Link, useNavigate } from 'react-router-dom';
import styles from './appBar.module.css';
import { BiTransfer } from 'react-icons/bi';

export const AppBar = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const {
    isUrlConverterPage,
    copyShareLinkSuccess,
    onCopyShareLinkSuccess,
    isDrawerOpened,
    setDrawerOpened,
    isTextVersionModalOpened,
    setTextVersionModalOpened,
    situation,
    onShare,
    onDownload,
    readonly,
    toggleReadonly,
  } = useAppBar();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const onOpenMoreToolsMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const onCloseMoreToolsMenu = () => {
    setAnchorEl(null);
  };

  return (
    <AppBarBase
      position="static"
      sx={{ flexGrow: 1, backgroundColor: '#222' }}
      className={styles.container}
    >
      <ToolbarBase>
        <Grid container spacing={1}>
          <Grid item sm={12} lg={5}>
            <Link to={'/'}>
              <Typography variant="h6" component="div" className={styles.title}>
                <Button
                  sx={{
                    color: '#FFF',
                    fontSize: { xs: '1rem', sm: '1.2rem' },
                  }}
                >
                  {t('menu.label')}
                </Button>
              </Typography>
            </Link>
          </Grid>

          <Grid container item xs={12} lg={7} spacing={0}>
            {!isUrlConverterPage && (
              <Grid item xs={12} sm={8}>
                <Stack
                  direction="row"
                  spacing={1}
                  sx={{ height: '100%' }}
                  alignItems="center"
                  justifyContent={{ lg: 'end' }}
                >
                  <Button
                    variant="contained"
                    color="success"
                    startIcon={<MdDownload />}
                    onClick={onDownload}
                  >
                    {t('menu.action.download')}
                  </Button>

                  <Button
                    variant="contained"
                    color="primary"
                    startIcon={<LuClipboardPaste />}
                    onClick={onShare}
                  >
                    {t('menu.action.share')}
                  </Button>

                  <IconButton
                    color="inherit"
                    aria-label={t('exportText.label')}
                    title={t('exportText.label')}
                    onClick={() => setTextVersionModalOpened(true)}
                    sx={{ mr: 2 }}
                    size="small"
                  >
                    <RxText />
                  </IconButton>
                </Stack>
              </Grid>
            )}

            <Grid item xs={12} sm={isUrlConverterPage ? 12 : 4}>
              <Stack
                direction="row"
                spacing={1}
                sx={{ height: '100%' }}
                alignItems="center"
                justifyContent={{ md: 'end' }}
              >
                {!isUrlConverterPage && (
                  <Button
                    variant="outlined"
                    color="primary"
                    startIcon={readonly ? <MdModeEdit /> : <MdOutlineRemoveRedEye />}
                    onClick={toggleReadonly}
                    sx={{
                      fontSize: '0.8rem',
                      backgroundColor: '#FFF',
                      '&:hover': {
                        backgroundColor: '#DDD',
                      },
                    }}
                  >
                    {readonly ? t('menu.action.edit') : t('menu.action.preview')}
                  </Button>
                )}

                <IconButton
                  aria-label={t('converter.label')}
                  id="more-tools"
                  aria-controls={open ? 'menu-more-tools' : undefined}
                  aria-expanded={open ? 'true' : undefined}
                  aria-haspopup="true"
                  onClick={onOpenMoreToolsMenu}
                  sx={{
                    color: '#FFF',
                  }}
                >
                  <MdMoreVert />
                </IconButton>

                <IconButton
                  edge="start"
                  color="inherit"
                  aria-label={t('settings.label')}
                  title={t('settings.label')}
                  onClick={() => setDrawerOpened(true)}
                  sx={{ mr: 2 }}
                  size="small"
                >
                  <BsGearFill />
                </IconButton>
              </Stack>
            </Grid>
          </Grid>
        </Grid>
      </ToolbarBase>

      <Menu
        id="menu-more-tools"
        MenuListProps={{
          'aria-labelledby': 'more-tools',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={onCloseMoreToolsMenu}
        TransitionComponent={Fade}
      >
        <MenuItem
          onClick={() => {
            onCloseMoreToolsMenu();
            navigate('/url-converter');
          }}
        >
          <Stack direction="row" alignItems="center" justifyContent="center" spacing={1}>
            <BiTransfer />
            <div>{t('converter.label')}</div>
          </Stack>
        </MenuItem>
      </Menu>

      <SettingDrawer opened={isDrawerOpened} onClose={() => setDrawerOpened(false)} />

      <TextOutput
        opened={isTextVersionModalOpened}
        onClose={() => setTextVersionModalOpened(false)}
        situation={situation}
      />

      <NotificationSnack
        opened={copyShareLinkSuccess}
        onDismiss={onCopyShareLinkSuccess}
        type="success"
      >
        {t('menu.shareLinkCopied')}
      </NotificationSnack>
    </AppBarBase>
  );
};
