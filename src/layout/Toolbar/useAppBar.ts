import { useAppDispatch } from '@/app/store';
import { selectSettings, toggleReadonly } from '@/features/settings/slices/settingSlice.ts';
import { SituationService } from '@/features/configurator/services/SituationService';
import { selectDeck } from '@/features/configurator/slices/deckSlice';
import { selectDiscardPile } from '@/features/configurator/slices/discardPileSlice';
import { selectFireworks } from '@/features/configurator/slices/fireworksSlice';
import { selectPlayers } from '@/features/configurator/slices/playersSlice';
import { download } from '@/shared/helpers/download';
import { htmlToPng } from '@/shared/helpers/image';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { useMatch } from 'react-router-dom';

export const useAppBar = () => {
  const dispatch = useAppDispatch();
  const { getShareUrl } = SituationService();

  const { readonly } = useSelector(selectSettings);
  const deck = useSelector(selectDeck);
  const fireworks = useSelector(selectFireworks);
  const discardPile = useSelector(selectDiscardPile);
  const players = useSelector(selectPlayers);

  const [copyShareLinkSuccess, setCopyShareLinkSuccess] = useState(false);
  const [isDrawerOpened, setDrawerOpened] = useState(false);
  const [isTextVersionModalOpened, setTextVersionModalOpened] = useState(false);

  const situation = {
    deck,
    fireworks,
    discardPile,
    players,
  };
  const isUrlConverterPage = useMatch('url-converter');

  const onShare = async () => {
    setCopyShareLinkSuccess(true);
    navigator.clipboard.writeText(await getShareUrl(situation, true));
  };

  const onDownload = () => {
    const htmlElement = document.getElementById('input-container');
    if (htmlElement) {
      htmlToPng(htmlElement).then((dataUrl: string) => download(dataUrl, 'hanabi-situation.png'));
    }
  };

  return {
    readonly,
    isUrlConverterPage,
    situation,
    onCopyShareLinkSuccess: () => setCopyShareLinkSuccess(false),
    onShare,
    onDownload,
    isDrawerOpened,
    setDrawerOpened,
    isTextVersionModalOpened,
    setTextVersionModalOpened,
    copyShareLinkSuccess,
    toggleReadonly: () => dispatch(toggleReadonly()),
  };
};
