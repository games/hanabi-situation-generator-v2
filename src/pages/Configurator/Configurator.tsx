import { Deck } from '@/features/configurator/components/Deck/Deck';
import { DiscardPile } from '@/features/configurator/components/DiscardPile/DiscardPile';
import { Fireworks } from '@/features/configurator/components/Fireworks/Fireworks';
import { Players } from '@/features/configurator/components/Players/Players';
import { useConfigurator } from '@/pages/Configurator/useConfigurator';
import { NotificationSnack } from '@/shared/components/NotificationSnack/NotificationSnack';
import { Container, Divider, Grid, Stack } from '@mui/material';
import React, { ReactNode } from 'react';
import { useTranslation } from 'react-i18next';
import styles from './configurator.module.css';
import { useSelector } from 'react-redux';
import { selectDeck } from '@/features/configurator/slices/deckSlice.ts';
import { selectFireworks } from '@/features/configurator/slices/fireworksSlice.ts';
import { selectDiscardPile } from '@/features/configurator/slices/discardPileSlice.ts';
import { selectPlayers } from '@/features/configurator/slices/playersSlice.ts';

export const Configurator = () => {
  const { t } = useTranslation();
  const { loadingError, dismissLoadingError, readonly, hideDeck } = useConfigurator();

  return (
    <Container sx={{ py: 2, position: 'relative' }}>
      <ColumnLayout
        readonly={readonly}
        columnLeftChildren={
          <Stack spacing={2} alignItems="start">
            <Stack direction="row" spacing={2} alignItems="start">
              {(!hideDeck || readonly) && <Deck readonly={readonly} />}
              <Fireworks readonly={readonly} />
            </Stack>

            <DiscardPile readonly={readonly} />
          </Stack>
        }
        columnRightChildren={<Players readonly={readonly} />}
      />

      <NotificationSnack opened={loadingError} onDismiss={dismissLoadingError} type="error">
        {t('situation.loadingError')}
      </NotificationSnack>
    </Container>
  );
};

const ColumnLayout = ({
  readonly,
  columnLeftChildren,
  columnRightChildren,
}: {
  readonly: boolean;
  columnLeftChildren: ReactNode;
  columnRightChildren: ReactNode;
}) => {
  const deck = useSelector(selectDeck);
  const fireworks = useSelector(selectFireworks);
  const discardPile = useSelector(selectDiscardPile);
  const players = useSelector(selectPlayers);

  const isLeftColumnEmpty =
    deck === undefined && fireworks === undefined && Object.keys(discardPile).length === 0;
  const isRightColumnEmpty = players === undefined;

  const isDividerNeeded = !readonly || (!isLeftColumnEmpty && !isRightColumnEmpty);
  return (
    <Stack
      gap={1}
      my={2}
      direction="column"
      divider={<Divider orientation="vertical" flexItem />}
      alignItems={readonly ? 'start' : undefined}
    >
      <Grid
        id="input-container"
        container
        spacing={1}
        className={styles.innerContainer}
        sx={{ width: 'auto', gap: 4 }}
      >
        {(!isLeftColumnEmpty || !readonly) && (
          <Grid item flex={1}>
            {columnLeftChildren}
          </Grid>
        )}

        <Grid
          sx={
            isDividerNeeded
              ? {
                  paddingLeft: { lg: '2rem !important' },
                  borderLeft: { lg: '2px solid #CCC' },
                }
              : undefined
          }
          item
          flex={1}
        >
          {columnRightChildren}
        </Grid>
      </Grid>
    </Stack>
  );
};
