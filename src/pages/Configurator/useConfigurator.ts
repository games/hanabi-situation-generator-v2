import { useAppDispatch } from '@/app/store';
import { selectSettings, setReadonly } from '@/features/settings/slices/settingSlice.ts';
import { SituationService } from '@/features/configurator/services/SituationService';
import { setNbRemainingCards } from '@/features/configurator/slices/deckSlice';
import { initDiscardPile } from '@/features/configurator/slices/discardPileSlice';
import { initFireworks } from '@/features/configurator/slices/fireworksSlice';
import { initPlayers } from '@/features/configurator/slices/playersSlice';
import { fromBase64 } from '@/shared/helpers/string';
import { useMetadata } from '@/shared/hooks/useMetadata';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

export const useConfigurator = () => {
  const { base64SituationData } = useParams();
  const dispatch = useAppDispatch();
  const { getDefaultSituation } = SituationService();
  useMetadata();

  const { readonly, hideDeck, defaultNbOfPlayers } = useSelector(selectSettings);

  const [loadingError, setLoadingError] = useState(false);

  useEffect(() => {
    try {
      let situation = getDefaultSituation(defaultNbOfPlayers);
      if (base64SituationData) {
        situation = JSON.parse(fromBase64(base64SituationData));
        // On charge toujours une situation en mode lecture seule
        dispatch(setReadonly(true));
      } else if (readonly) {
        // Si on a une situation vierge, on ne veut pas la lecture seule
        dispatch(setReadonly(false));
      }

      dispatch(setNbRemainingCards(situation.deck?.nbRemainingCards));
      dispatch(initFireworks(situation.fireworks));
      dispatch(initDiscardPile(situation.discardPile));
      dispatch(initPlayers(situation.players));
    } catch {
      setLoadingError(true);
    }
  }, [base64SituationData]);

  return {
    loadingError,
    readonly,
    dismissLoadingError: () => setLoadingError(false),
    hideDeck,
  };
};
