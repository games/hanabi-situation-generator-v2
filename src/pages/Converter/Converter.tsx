import { ConverterService as ConverterService } from '@/features/url-converter/services/ConverterService';
import { extractUrls } from '@/shared/helpers/url';
import { Alert, Button, Container, Grid, Stack, TextField, Typography } from '@mui/material';
import React, { useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import { RiExternalLinkFill } from 'react-icons/ri';
import styles from './converter.module.css';

export const Converter = () => {
  const { t } = useTranslation();
  const { convertUrl } = ConverterService();

  const [newUrls, setNewUrls] = useState<string[]>([]);

  return (
    <Container sx={{ py: 2, position: 'relative' }}>
      <Stack gap={1} direction="column" width="100%">
        <Typography variant="h2" component="div">
          {t('converter.label')}
        </Typography>

        <Alert severity="info">
          <Trans i18nKey="converter.legacyUrlInfo" />
        </Alert>

        <p>{t('converter.description')}</p>

        <Grid container spacing={1}>
          <Grid item sm={12} lg={6}>
            <TextField
              id="old-urls"
              className={styles.textarea}
              label={t('converter.legacyUrl')}
              multiline
              onChange={async (event) => {
                setNewUrls(
                  event.target.value
                    ? await Promise.all(extractUrls(event.target.value).map((url) => convertUrl(url, true)))
                    : [],
                );
              }}
              minRows={4}
            />
          </Grid>

          <Grid item sm={12} lg={6}>
            <Stack
              direction="row"
              spacing={1}
              sx={{ height: '100%' }}
              justifyContent={{ lg: 'end' }}
              alignItems="flex-start"
            >
              <TextField
                id="new-urls"
                className={styles.textarea}
                label={t('converter.newUrl')}
                value={newUrls.join('\n')}
                multiline
                minRows={4}
                disabled
              />

              {newUrls.length > 0 && (
                <Button
                  variant="contained"
                  color="success"
                  endIcon={<RiExternalLinkFill />}
                  href={newUrls[0] ?? ''}
                  target="_blank"
                >
                  {t('converter.action.open')}
                </Button>
              )}
            </Stack>
          </Grid>
        </Grid>
      </Stack>
    </Container>
  );
};
