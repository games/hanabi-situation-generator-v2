import { Fade, Paper, Stack } from '@mui/material';
import { ReactNode } from 'react';
import styles from './actionMenu.module.css';

type Props = {
  children: ReactNode;
  opened: boolean;
  vertical?: boolean;
  outside?: boolean;
  opposite?: boolean;
  floating?: boolean;
};

export const ActionMenu = ({
  children,
  opened,
  opposite,
  vertical = true,
  outside = true,
  floating = true,
}: Props) => {
  return (
    <>
      {floating ? (
        <Fade in={opened} timeout={opened ? 500 : undefined}>
          <Paper
            className={`
              ${styles.container} 
              ${floating ? styles.floating : ''}
              ${outside ? styles.outside : ''}
              ${vertical ? styles.vertical : ''}
              ${opposite ? styles.opposite : ''}
              ${opened ? styles.opened : styles.closed}
            `}
            variant="outlined"
          >
            <Stack direction={vertical ? 'column' : 'row'} justifyContent="end">
              {children}
            </Stack>
          </Paper>
        </Fade>
      ) : (
        <Stack
          direction={vertical ? 'column' : 'row'}
          className={styles.container}
          justifyContent="end"
        >
          {opened && <>{children}</>}
        </Stack>
      )}
    </>
  );
};
