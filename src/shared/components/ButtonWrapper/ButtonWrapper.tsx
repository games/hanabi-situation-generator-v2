import { Button } from '@mui/material';
import React, { ReactNode } from 'react';

type Props = {
  onClick?: () => void;
  title?: string;
  children: ReactNode;
};

export const ButtonWrapper = ({ onClick, title, children }: Props) => {
  return (
    <>
      {onClick ? (
        <Button
          sx={{
            padding: 0,
            height: '100%',
            width: '100%',
            minWidth: 'initial',
            color: 'black',
          }}
          title={title}
          onClick={onClick}
        >
          {children}
        </Button>
      ) : (
        <>{children}</>
      )}
    </>
  );
};
