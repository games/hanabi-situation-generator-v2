import { Box, Card } from '@mui/material';
import React, { ReactNode, useRef } from 'react';
import styles from './flexWrapper.module.css';

type Props = {
  children: ReactNode;
};

export const FlexWrapper = ({ children }: Props) => {
  const container = useRef(null);

  return (
    <Card
      variant="outlined"
      className={`
        ${styles.container} 
        flexBox
      `}
      ref={container}
    >
      <Box className={`${styles.innerContainer} flexBox`}>{children}</Box>
    </Card>
  );
};
