import { ClickAwayListener, IconButton, Paper } from '@mui/material';
import React, { ReactNode, useEffect, useId } from 'react';
import { useTranslation } from 'react-i18next';
import { MdClose } from 'react-icons/md';
import styles from './floatingForm.module.css';

type Props = {
  children: ReactNode;
  onBlurOrDismiss: () => void;
  position?: 'left' | 'right';
};

export const FloatingForm = ({ children, onBlurOrDismiss, position = 'right' }: Props) => {
  const { t } = useTranslation();
  const id = useId();

  // Focus automatique dans la popin
  useEffect(() => {
    if (id) {
      document.getElementById(id)?.focus();
    }
  }, [id]);

  return (
    <ClickAwayListener onClickAway={onBlurOrDismiss}>
      <Paper className={`${styles.container} ${styles[position]}`}>
        <IconButton
          className={styles.closeButton}
          onClick={onBlurOrDismiss}
          size="small"
          color="error"
          title={t('_shared.action.close')}
          id={id}
        >
          <MdClose />
        </IconButton>
        {children}
      </Paper>
    </ClickAwayListener>
  );
};
