import { Alert, Snackbar } from '@mui/material';
import React, { ReactNode } from 'react';
import { useTranslation } from 'react-i18next';

type Props = {
  children: ReactNode;
  opened: boolean;
  onDismiss: () => void;
  type: 'info' | 'success' | 'error';
};

export const NotificationSnack = ({ children, opened, onDismiss, type }: Props) => {
  const { t } = useTranslation();
  return (
    <Snackbar open={opened} autoHideDuration={6000} onClose={onDismiss}>
      <Alert
        onClose={onDismiss}
        severity={type}
        variant="filled"
        sx={{ width: '100%' }}
        closeText={t('_shared.action.close')}
      >
        {children}
      </Alert>
    </Snackbar>
  );
};
