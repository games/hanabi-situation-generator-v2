import { IconButton } from '@mui/material';
import React from 'react';
import {
  IoMdArrowRoundBack,
  IoMdArrowRoundDown,
  IoMdArrowRoundForward,
  IoMdArrowRoundUp,
} from 'react-icons/io';

type Props = {
  title: string;
  onClick: () => void;
  direction: 'left' | 'top' | 'right' | 'bottom';
};

export const MoveButton = ({ title, onClick, direction }: Props) => {
  return (
    <IconButton onClick={onClick} size="small" color="primary" title={title}>
      {direction === 'left' && <IoMdArrowRoundBack />}
      {direction === 'top' && <IoMdArrowRoundUp />}
      {direction === 'right' && <IoMdArrowRoundForward />}
      {direction === 'bottom' && <IoMdArrowRoundDown />}
    </IconButton>
  );
};
