import { IconButton } from '@mui/material';
import React from 'react';
import { MdDelete } from 'react-icons/md';

type Props = {
  title: string;
  onClick: () => void;
};

export const RemoveButton = ({ title, onClick }: Props) => {
  return (
    <IconButton onClick={onClick} size="small" color="error" title={title}>
      <MdDelete />
    </IconButton>
  );
};
