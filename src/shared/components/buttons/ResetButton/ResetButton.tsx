import { IconButton } from '@mui/material';
import React from 'react';
import { BsRecycle } from 'react-icons/bs';

type Props = {
  title: string;
  onClick: () => void;
};

export const ResetButton = ({ title, onClick }: Props) => {
  return (
    <IconButton onClick={onClick} size="small" color="primary" title={title}>
      <BsRecycle />
    </IconButton>
  );
};
