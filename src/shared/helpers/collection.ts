export const getItem = <T>(list: T[], index: number): T => {
  if (index < -1) {
    throw new Error('Index cannot be negative');
  }
  if (index > Number(list?.length)) {
    throw new Error(`Index ${index} is greater than the array length ${list?.length}`);
  }

  return list[index];
};

export const move = <T>(list: T[], currentIndex: number, newIndex: number): T[] => {
  const clone = [...list];
  const element = clone[currentIndex];
  clone.splice(currentIndex, 1);
  clone.splice(newIndex, 0, element);
  return clone;
};

export const removeDuplicated = <T>(list: T[]): T[] => {
  return Array.from(new Set(list));
};
