import { toPng } from 'html-to-image';

export const htmlToPng = async (htmlElement: HTMLElement) => {
  return toPng(htmlElement, { quality: 0.95 });
};
