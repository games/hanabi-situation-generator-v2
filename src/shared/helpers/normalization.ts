import { toBase64 } from '@/shared/helpers/string';

export const normalizeAsJsonBase64 = (data: object): string => {
  return toBase64(JSON.stringify(data));
};
