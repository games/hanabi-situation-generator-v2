export const toBase64 = (string: string) => btoa(string);

export const fromBase64 = (base64String: string) => atob(base64String);
