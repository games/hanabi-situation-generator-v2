export const getBaseUrl = () => {
  return import.meta.env.VITE_BASE_URL;
};

export const getUrlQueryParams = (url: string) => new URL(url).searchParams;

export const buildQueryString = (data: { [key: string]: string } = {}) => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&');
};

export const extractUrls = (rawUrls: string): string[] => {
  const regexp = /(http[^\n|^\r]*)/gm;

  return rawUrls.match(regexp) || [];
};
