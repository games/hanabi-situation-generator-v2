import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

export const useMetadata = () => {
  const { i18n, t } = useTranslation();

  useEffect(() => {
    document.title = t('application.title');
    document.documentElement.lang = i18n.language;
  }, []);
};
